<?php require_once('header.php');?>
	<div class="main">
		<div class="wrap">
			<div class="content_top">
				<div class="blog">
					<h2>Blogs</h2>
					<div class="blog-leftgrids">
						<div class="image group">
							<div class="grid images_3_of_1">
								<a href="#">
									<img src="images/blog-img2.jpg" alt="">
								</a>
							</div>
							<div class="grid blog-desc">
								<h4>
									<span>Duis aute irure dolor cillum dolore</span>
								</h4>
								<h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By
									<a href="#" class="post">Lorem ipsum</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
									ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
									esse cillum dolore eu fugiat nulla pariatur.
									<span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
								</p>
								<a href="#" class="button">Read More</a>
							</div>
						</div>
					</div>
					<div class="blog-leftgrids">
						<div class="image group">
							<div class="grid images_3_of_1">
								<a href="#">
									<img src="images/blog-img1.jpg" alt="">
								</a>
							</div>
							<div class="grid blog-desc">
								<h4>
									<span>Excepteur sint non proident</span>
								</h4>
								<h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By
									<a href="#" class="post">Lorem ipsum</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
									ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
									esse cillum dolore eu fugiat nulla pariatur.
									<span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
								</p>
								<a href="#" class="button">Read More</a>
							</div>
						</div>
					</div>
					<div class="blog-leftgrids">
						<div class="image group">
							<div class="grid images_3_of_1">
								<a href="#">
									<img src="images/event-img1.jpg" alt="">
								</a>
							</div>
							<div class="grid blog-desc">
								<h4>
									<span>Duis aute irure dolor cillum dolore</span>
								</h4>
								<h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By
									<a href="#" class="post">Lorem ipsum</a>
								</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
									ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
									esse cillum dolore eu fugiat nulla pariatur.
									<span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
								</p>
								<a href="#" class="button">Read More</a>
							</div>
						</div>
					</div>
					<div class="content-pagenation">
						<li>
							<a href="#">Frist</a>
						</li>
						<li class="active">
							<a href="#">1</a>
						</li>
						<li>
							<a href="#">2</a>
						</li>
						<li>
							<a href="#">3</a>
						</li>
						<li>
							<span>....</span>
						</li>
						<li>
							<a href="#">Last</a>
						</li>
						<div class="clear"> </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="wrap">
			<div class="half-footer" style="margin-left:0">
				<ul class="feeds">
					<h3>Our Latest feeds</h3>
					<li>
						<a href="index.php">Lorem ipsum dolor consectetur adiping</a>
					</li>
					<li>
						<a href="blog.php">Nunc sagittis mollis eros, at venenatis</a>
					</li>
					<li>
						<a href="blog.php">Morbi nec dolor ipsum vel congugue</a>
					</li>
					<li>
						<a href="blog.php">Nullam a odio ipsum, at sodales lorem.</a>
					</li>
					<li>
						<a href="contact.php">Nullam imperdiet vulputate congugue</a>
					</li>
				</ul>
				<div class="footer-pic">
					<img src="images/f-icon.png" alt="">
				</div>
				<div class="clear"></div>
			</div>
			<div class="half-footer" style="border:none">
				<ul class="adress">
					<h3>Catch on</h3>
					<li>
						<a href="index.php">Events Club</a>
					</li>
					<li>
						<a href="blog.php">West Coast, CA</a>
					</li>
					<li>
						<a href="blog.php">Facebook - I Music</a>
					</li>
					<li>
						<a href="blog.php">Login</a>
					</li>
					<li>
						<a href="mailto:example@mail.com">yourname(at)companyname.com</a>
					</li>
				</ul>
				<div class="footer-pic">
					<img src="images/foot-icon.png" alt="">
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="copy">
		<p>© 2013 Events Club.All Rights Reserved | Design by
			<a href="http://w3layouts.com">W3Layouts</a>
		</p>
	</div>
</body>

</html>