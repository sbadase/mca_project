<?php
require_once('header.php');
if(!isset($_SESSION['user_data'])){
    header('Location: login.php');
}

if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php
}



if(isset($_POST['admin_add_new_event_tickets_submit'])){
    $error=0;
    if(empty($_POST['event_id'])){
        $error=1;
    }
    if(empty($_POST['content'])){
        $error=1;
    }
    if($error == 0){
        $content = addslashes($_POST['content']);
        $content;
        $adminInsertQuery = "INSERT INTO customer_feedback_or_inquiries (event_id, feedback, approval_status,parent_id,is_admin,date,customer_id,record_type)
VALUES ('" . $_POST['event_id'] . "', '" . $content . "', '0','0','0','" . date("Y-m-d h:i:sa") . "','" . $_SESSION['user_data']['id'] . "',2)";
        mysqli_query($conn, $adminInsertQuery);
        header('Location: give_feedback.php?msg=Thanks For Your Feedback !!!!');

    }
}
?>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http
      quiv="X-UA-Compatible" content="ie=edge">
<title>Carbon - Admin Template</title>
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/simple-line-icons/css/simple-line-icons.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/font-awesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/css/styles.css">
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Share Your FeedBack !! We Like To here You !!
                            </div>
                            <form method="post" action="give_feedback.php" class="form-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Select Event</label>
                                                <select onchange="window.location='<?php echo $_SERVER['SCRIPT_NAME'];?>?event_id=' + this.value" id="event_tickets_event_id" name="event_id" class="form-control" required>
                                                    <option value="9999999">General(About Site)</option>
                                                    <?php
                                                    $checkAlreadyExistQuery='select * from registered_event';
                                                    $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                    if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                        while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                            ?>

                                                            <option value="<?php echo $data['event_id']; ?>"  <?php if(isset($_GET['event_id'])&& !empty($_GET['event_id']) && is_numeric($_GET['event_id'])&& $_GET['event_id']==$data['event_id']){ echo "selected";}?>  ><?php echo $data['name']; ?></option>
                                                            <?php
                                                        } }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
      <textarea cols="80" rows="10" id="content" name="content" >
        &lt;h1&gt;Article Title&lt;/h1&gt;
        &lt;p&gt;Here's some sample text&lt;/p&gt;
      </textarea>
                                        <script type="text/javascript">
                                            CKEDITOR.replace('content' );
                                        </script>

                                    </div>
                                    <br>
                                    <input type="submit" name="admin_add_new_event_tickets_submit" value="Share FeedBack">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
