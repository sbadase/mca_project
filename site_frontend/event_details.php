<?php require_once('header.php');?>
<style type="text/css">

	.mynewbutton{
		font-size: 1em;
		text-transform: uppercase;
		margin-right: 496px;
		color:#FFF;
		cursor:pointer;
		background:#CA009C;
		text-decoration: none;
		outline: 0;
		border:none;
		-webkit-transition: all 0.5s ease-in-out;
		-moz-transition: all 0.5s ease-in-out;
		-o-transition: all 0.5s ease-in-out;
		transition: all 0.5s ease-in-out;
		font-family:Arial, Helvetica, sans-serif;
		position:absolute;
		right:0;
		-webkit-apperance:none;
	}
</style>
	<div class="main">
		<div class="wrap">
			<div class="content_top">
				<div class="blog">
					<h2>Event Detail</h2>
					<?php $frontendSelectQuery = 'select * from registered_event where event_id ='.$_GET['event_id'];
					$frontendSelectQueryResult = mysqli_query($conn, $frontendSelectQuery);
					if (mysqli_num_rows($frontendSelectQueryResult) == 0) { ?>

						<div class="grid blog-desc">
							<h4>
								<span>Event Does Not Exists</span>
							</h4>
						</div>
					<?php }else {
						while ($row = mysqli_fetch_assoc($frontendSelectQueryResult)) {
							$image_name = $row['event_banner_image'];
							$file = '../site_backend/'.$image_name; // 'images/'.$file (physical path)

							if (!file_exists($file)) {
								$image_name = 'images/no_image_gal.jpg';
							}else{
								$image_name = '../site_backend/'.$image_name;
							}
						?>
						<div class="blog-leftgrids">
						<div class="image group">
							<div class="grid images_3_of_1">
								<a href="#">
									<img src="<?php echo $image_name;?>" alt="">
								</a>
							</div>
							<div class="grid blog-desc">
								<h4>
									<span><?php echo $row['name']; ?> : Ticket Price : <?php echo $row['price']; ?> </span>
								</h4>
								<h4>Event Performed By : <?php echo $row['chief_guest']; ?>
								</h4>
								<h4><?php echo $row['start_date']; ?> - <?php echo $row['end_date']; ?>&nbsp;&nbsp; posted By
									<a href="#" class="post"><?php echo $row['event_by']; ?></a>
								</h4>
								<p><?php echo $row['description']; ?>
								</p>
	<span>
											<input class="mynewbutton" onclick="window.location = 'book_ticket.php?gal_status=0&&event_id=<?php echo $_GET['event_id'] ?>&&customer_id=<?php echo $_SESSION['user_data']['id'] ?>'";  value="Book Ticket" type="submit">
										</span>

							</div>
						</div>

					</div>
					<?php } }?>
				</div>
			</div>
		</div>
	</div>



<?php require_once('footer.php');?>