<?php require_once('header.php');?>
<?php
if(!isset($_SESSION['user_data'])){
    header('Location: login.php');
}
?>
    <style type="text/css">
        .mybutton{
            padding: 8px 15px;
            font-size: 1em;
            margin-right: 136px;
            text-transform: uppercase;
            color:#FFF;
            cursor:pointer;
            background:#CA009C;
            text-decoration: none;
            outline: 0;
            border:none;
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
            font-family:Arial, Helvetica, sans-serif;
            position:absolute;
            right:0;
            -webkit-apperance:none;
        }

    </style>
<?php
if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php

}
    if(isset($_POST['book_ticket_submit'])){
    $error=0;
    if(empty($_POST['password'])){
    $error=1;
    }
    if(empty($_POST['phone_number'])){
    $error=1;
    }
    if(empty($_POST['type'])){
        $error=1;
    }
    if($error == 0){

        $userUpdateQuery = "UPDATE frontend_user SET phone_number='".$_POST['phone_number']."',type='".$_POST['type']."',password = '".md5($_POST['password'])."' WHERE id=".$_SESSION['user_data']['id'];
        mysqli_query($conn, $userUpdateQuery);
        header('Location: edit_account.php');


    }else{

        ?>
        <script>
            alert("Form not Complete Edit Failed");
        </script>
        <?php
    }
    }
?>
    <div class="main" align="center">
        <div class="wrap">
            <div class="content_top">
                <div class="contact">
                    <div class="section group">
                        <div class=" contact_1_of_3">
                            <div class="contact-form">
                                <h3>Update Details </h3>
                                <form method="post" action="edit_account.php" class="form-horizontal">
                                    <div>
										<span>
											<h3>User Name /Email : <?php echo $_SESSION['user_data']['user_name'] ?></h3>
										</span>
                                    </div>
                                    <div>
										<span>
											<h3>Password :</h3> <input type="password" name="password" class="textbox" value="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required>
										</span>
                                    </div>
                                    <div>
										<span>
											<h3>Contact Number :</h3> <input type="text" name="phone_number" class="textbox" value=" <?php echo $_SESSION['user_data']['phone_number'] ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required>
										</span>
                                    </div>
                                    <div>
										<span>
											<h3>Want To Do :</h3>
                                            <select name="type">
                                                <option value="1">Business</option>
                                                <option value="2">Normal</option>
                                            </select>
										</span>
                                    </div>
                                    <div>
										<span>
											<input type="submit" name="book_ticket_submit" align="center" class="mybutton" value="Submit">
										</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once('footer.php');?>