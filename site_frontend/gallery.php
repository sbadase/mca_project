<?php require_once('header.php');?>
	<div class="main">
		<div class="wrap">
			<div class="content_top">
				<div class="gallery">
					<h3>Latest Photos</h3>
					<?php $frontendSelectQuery = 'select * from image_gallery where status = 1 and header_slider=0';
					$frontendSelectQueryResult = mysqli_query($conn, $frontendSelectQuery);
					if (mysqli_num_rows($frontendSelectQueryResult) == 0) { ?>
						<div class="section group">
							<div class="grid_1_of_4 images_1_of_4">
								<a href="images/no_image_gal.jpg" class="swipebox" title="Image Title">
									<img src="images/no_image_gal.jpg" alt="">
									<span class="zoom-icon"></span>
								</a>
							</div>
							<div class="grid_1_of_4 images_1_of_4">
								<a href="images/no_image_gal.jpg" class="swipebox" title="Image Title">
									<img src="images/no_image_gal.jpg" alt="">
									<span class="zoom-icon"></span>
								</a>
							</div>
							<div class="grid_1_of_4 images_1_of_4">
								<a href="images/no_image_gal.jpg" class="swipebox" title="Image Title">
									<img src="images/no_image_gal.jpg" alt="">
									<span class="zoom-icon"></span>
								</a>
							</div>
							<div class="grid_1_of_4 images_1_of_4">
								<a href="images/no_image_gal.jpg" class="swipebox" title="Image Title">
									<img src="images/no_image_gal.jpg" alt="">
									<span class="zoom-icon"></span>
								</a>
							</div>
						</div>
					<?php }else {
						?> <div class="section group"> <?php
						$count = 1;
						while ($row = mysqli_fetch_assoc($frontendSelectQueryResult)) {
							if( $count % 5 == 0 ){
								?>
								</div><div class="section group">
								<?php
							}
							$image_name = $row['image_name'];

								$file = 'images/'.$image_name; // 'images/'.$file (physical path)

							if (!file_exists($file)) {
								$image_name = 'no_image_gal.jpg';
							}
							?>
						<div class="grid_1_of_4 images_1_of_4">
							<a href="images/<?php echo $image_name; ?>" class="swipebox" title="Image Title">
								<img src="images/<?php echo $image_name; ?>" alt="">
								<span class="zoom-icon"></span>
							</a>
						</div>
						<?php $count++;}
					}?>
				</div>
			</div>
		</div>
	</div>
<?php require_once('footer.php');?>