<?php
require_once('../config.php');
?>
<!DOCTYPE HTML>
<html>

<head>
	<title>Events Club a Entertainment Category Website Template | Home :: w3layouts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/slider.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
	<script type="text/javascript">
		$(window).load(function () {
			$('#slider').nivoSlider();
		});
	</script>
	<!------ Light Box ------>
	<link rel="stylesheet" href="css/swipebox.css">
	<script src="js/jquery.swipebox.min.js"></script>
	<script type="text/javascript">
		jQuery(function ($) {
			$(".swipebox").swipebox();
		});
	</script>
	<!------ Eng Light Box ------>

	<!------ Light Box ------>
	<link rel="stylesheet" href="css/swipebox.css">
	<script src="js/jquery.swipebox.min.js"></script>
	<script type="text/javascript">
		jQuery(function ($) {
			$(".swipebox").swipebox();
		});
	</script>
	<!------ Eng Light Box ------>
</head>
<body>
<div class="header">
	<div class="header_top">
		<div class="wrap">
			<div class="logo">
				<a href="index.php">
					<img src="images/logo.png" alt="" />
				</a>
			</div>
			<div class="menu">

				<ul>
					<?php if(isset($_SESSION['user_data']) && !empty($_SESSION['user_data'])){
						?>
						<li class="active">
							<a href="index.php">Welcome<br><?php echo $_SESSION['user_data']['user_name'] ?></a>
						</li>
						<li class="active">
							<a href="logout.php">LOGOUT</a>
						</li>
						<li class="active">
							<a href="edit_account.php">EDIT ACCOUNT</a>
						</li>
						<li class="active">
							<a href="your_tickets.php?gal_status=0">MY BOOKINGS</a>
						</li>
						<li class="active">
							<a href="give_feedback.php?gal_status=0">Share Your Feedback</a>
						</li>
						<li>
							<a href="customers_inquiries.php">Your Inquiries and Status</a>
						</li>
						<li>
							<a href="tickets_by_customer.php">Support Tickets</a>
						</li>
					<?php
					}else {?>
						<li class="active">
							<a href="register_user.php">Register Now</a>
						</li>
					<li class="active">
						<a href="login.php">Login</a>
					</li>
					<?php } ?>
					<li class="active">
						<a href="index.php">HOME</a>
					</li>
					<li>
						<a href="about.php">ABOUT</a>
					</li>
					<li>
						<a href="events.php">EVENTS</a>
					</li>
					<li>
						<a href="gallery.php">GALLERY</a>
					</li>
					<li>
						<a href="blog.php">BLOG</a>
					</li>
					<li>
						<a href="contact.php">CONTACT</a>
					</li>
					<li>
						<a href="customers_feedback.php">Customers Feedback</a>
					</li>

					<div class="clear"></div>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!------ Slider ------------>
	<?php if(!isset($_GET['gal_status'])){?>
	<div class="wrap">
		<div class="slider">
			<div class="slider-wrapper theme-default">
				<div id="slider" class="nivoSlider">
					<?php $frontendSelectQuery = 'select * from image_gallery where status = 1 and header_slider=1';
					$frontendSelectQueryResult = mysqli_query($conn, $frontendSelectQuery);
					if (mysqli_num_rows($frontendSelectQueryResult) == 0) { ?>
						<img src="images/default.jpg" data-thumb="images/default.jpg" alt="" />
					<?php }else {
					while ($row = mysqli_fetch_assoc($frontendSelectQueryResult)) {
                    $image_name = $row['image_name'];
						$file = 'images/'.$image_name; // 'images/'.$file (physical path)

						if (!file_exists($file)) {
							$image_name = 'no_image.jpg';
						}
					?>
					<img src="images/<?php echo $image_name; ?>" data-thumb="images/<?php echo $image_name; ?>" alt="" />
					<?php }
					}?>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
	<!------End Slider ------------>
</div>