<?php require_once('header.php');?>
	<style>
		.test {
			width: 70em;
			word-wrap: break-word;
			font-family: "Comic Sans MS", cursive, sans-serif;
			text-align: justify;
			color: lightpink;
		}
	</style>
	<div class="main">
		<div class="wrap">
			<div class="test">
				<p>Event management


				<p>This article has multiple issues. Please help improve it or discuss these issues on the talk page. (Learn how and when to remove these template messages)
				<p>	This article needs additional citations for verification. (June 2007)
				<p>This article includes a list of references, but its sources remain unclear because it has insufficient inline citations. (April 2016)
				<p>	Event management is the application of project management to the creation and development of large scale events such as festivals, conferences, ceremonies, weddings, formal parties, concerts, or conventions. It involves studying the brand, identifying its target audience, devising the event concept, and coordinating the technical aspects before actually launching the event.[1]

				<p>The process of planning and coordinating the event is usually referred to as event planning and which can include budgeting, scheduling, site selection, acquiring necessary permits, coordinating transportation and parking, arranging for speakers or entertainers, arranging decor, event security, catering, coordinating with third party vendors, and emergency plans. Each event is different in its nature so process of planning & execution of each event differs on basis of type of event.

				<p>The events industry now includes events of all sizes from the Olympics down to business breakfast meetings. Many industries, charitable organizations, and interest groups hold events in order to market themselves, build business relationships, raise money, or celebrate achievement.

				<p>Marketing tool
				<p>Event management might be a tool for strategic marketing and communication, used by companies of every size. Companies can benefit from promotional events as a way to communicate with current and potential customers. For instance, these advertising-focused events can occur as press conferences, promotional events, or product launches.

				<p>Event managers may also use traditional news media in order to target their audience, hoping to generate media coverage which will reach thousands or millions of people. They can also invite their audience to their events and reach them at the actual event.

				<p>Event manager
				The event manager is the person who plans and executes the event, taking responsibility for the creative, technical, and logistical elements. This includes overall event design, brand building, marketing and communication strategy, audio-visual production, script writing, logistics, budgeting, negotiation, and client service.

				<p>Event venue
				An event venue may be an onsite or offsite location. The event manager is usually not responsible for operations at rented event or entertainment venues, but will monitor all aspects of the event on site. Some of the tasks listed in the introduction may pass to the venue, but usually at a cost.

				<p>Corporate event managers book event venues to host corporate meetings, conferences, networking events, trade shows, product launches, team building retreats or training sessions in a more tailored environment.

				<p>Sustainability
				<p>Sustainable event management (also known as event greening) is the process used to produce an event with particular concern for environmental, economic, and social issues.[2] Sustainability in event management incorporates socially and environmentally responsible decision making into the planning, organization and implementation of, and participation in, an event. It involves including sustainable development principles and practices in all levels of event organization, and aims to ensure that an event is hosted responsibly. It represents the total package of interventions at an event, and needs to be done in an integrated manner. Event greening should start at the inception of the project, and should involve all the key role players, such as clients, organizers, venues, sub-contractors, and suppliers.

				<p>Technology
				Event management software companies provide event planners with software tools to handle many common activities such as delegate registration, hotel booking, travel booking, or allocation of exhibition floorspace.

				<p>A recent trend in event technology is the use of mobile apps for events. Event mobile apps have a range of uses. They can be used to hold relatively static information such as the agenda, speaker biographies, and general FAQs. They can also encourage audience participation and engagement through interactive tools such as live voting/polling, submitting questions to speakers during Q&A, or building live interactive "word clouds". Mobile event apps can also be used by event organisers as a means of communication. Organisers can communicate with participants through the use of alerts, notifications, and push messages. They can also be used to collect feedback from the participants through the use of surveys in app. Going a step further, some mobile event apps can also help participants to engage with each other, with sponsors, and with the organisers with built-in networking functionality.

				<p>Education
				<p>	There are an increasing number of universities which offer courses in event management, including diplomas and graduate degrees. In addition to these academic courses, there are many associations and societies that provide courses on the various aspects of the industry. Study includes organizational skills, technical knowledge, public relations, marketing, advertising, catering, logistics, decor, glamor identity, human relations, study of law and licenses, risk management, budgeting, and the study of allied industries like television, other media, and several other areas. Certification can be acquired from various sources to obtain designations such as Certified Trade Show Marketer (CTSM), Certified Manager of Exhibits (CME), Certified in Exhibition Management (CEM), Global Certification in Meeting Management (CMM), Certified Meeting Professional (CMP), Global Certificate in Event Design (EDC), Certified Special Event Professional (CSEP), and Certified Quality Event Planner (CQEP).

			</div>
			<div class="content_bottom">
				<div class="section group">
					<div class="col_1_of_3 span_1_of_3">
						<h3>Whats New In Gallary</h3>
						<a href="gallery.php">
							<img src="images/no_image_gal.jpg" alt="" />
						</a>

						<a href="gallery.php" class="button">See All</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php require_once('footer.php');?>