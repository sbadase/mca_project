<?php
require_once('header.php');
if(!isset($_SESSION['user_data'])){
    header('Location: login.php');
}
if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php
    header('Location: your_tickets.php');
}

?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http
      quiv="X-UA-Compatible" content="ie=edge">
<title>Carbon - Admin Template</title>
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/simple-line-icons/css/simple-line-icons.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/font-awesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/css/styles.css">
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                Your Bookings
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th> FeedBack ID</th>
                                    <th> Customer ID</th>
                                    <th>Feed Back for <br>
                                        Event/General(About Site)
                                    </th>
                                    <th>Feed Back</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $adminSelectQuery = 'select * from customer_feedback_or_inquiries where record_type = 2 and approval_status = 1';
                                $adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
                                if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
                                    <tr>
                                        <td colspan="6" align="center">No Data Found</td>
                                    </tr>
                                <?php }else {
                                    while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['feedback_id']?></td>
                                            <td class="text-nowrap">
                                                <?php

                                                $checkAlreadyExistQuery='select * from frontend_user where status=1 and id ='.$row['customer_id'];
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <p class="form-control-plaintext"><?php echo $row['customer_id'].'/'.$data['user_name']?></p>
                                                        <?php
                                                    } }  ?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php

                                                $checkAlreadyExistQuery='select * from registered_event where event_status=1 and event_id ='.$row['event_id'];
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <p class="form-control-plaintext"><?php echo $row['event_id'].'/'.$data['name']?></p>
                                                        <?php
                                                    } }else{
                                                    ?>
                                                    <p class="form-control-plaintext">General(About Site)</p>
                                                    <?php

                                                }  ?>
                                            </td>
                                            <td><?php echo $row['feedback'];?></td>
                       </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?>
