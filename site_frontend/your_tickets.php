<?php
require_once('header.php');
if(!isset($_SESSION['user_data'])){
    header('Location: login.php');
}
if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php
    header('Location: your_tickets.php');
}

?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http
      quiv="X-UA-Compatible" content="ie=edge">
<title>Carbon - Admin Template</title>
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/simple-line-icons/css/simple-line-icons.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/font-awesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/css/styles.css">
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Your Bookings
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Ticket ID</th>
                                    <th>Event ID/Name</th>
                                    <th>Ticket Code</th>
                                    <th>Ticket Count</th>
                                    <th>Ticket Unit Price</th>
                                    <th>Total billed</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  $adminSelectQuery = 'select * from event_tickets where customer_id='.$_SESSION['user_data']['id'];
                                $adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
                                if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
                                    <tr>
                                        <td colspan="6" align="center">No Data Found</td>
                                    </tr>
                                <?php }else {
                                    while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['ticket_id']?></td>
                                            <td class="text-nowrap">
                                                <?php

                                                $checkAlreadyExistQuery='select * from registered_event where event_status=1 and event_id ='.$row['event_id'];
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <p class="form-control-plaintext"><?php echo $row['event_id'].'/'.$data['name']?></p>
                                                        <?php
                                                    } }  ?>
                                            </td>
                                            <td id="printablediv_<?php echo $row['ticket_id'] ?>"><?php echo $row['ticket_readable_string'] ?><br><iframe src="../<?php echo $row['ticket_code'] ?>"></iframe><br>
                                                <input type="button" value="Print Ticket" onclick="javascript:printDiv('printablediv_<?php echo $row['ticket_id'] ?>')" /></td>
                                            <td><?php echo $row['number_of_tickets']; ?></td>
                                            <td><?php echo $row['unit_price']; ?></td>
                                            <td><?php echo $row['total_billed']; ?></td>
                                        </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?>
<script type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;
        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
        //Print Page
        window.print();
        //Restore orignal HTML
        document.body.innerHTML = oldPage;

    }


</script>