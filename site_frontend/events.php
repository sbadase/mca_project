<?php require_once('header.php');?>
	<div class="main">
		<div class="wrap">
			<div class="content_top">
				<div class="events">
					<h2>Upcoming Events</h2>
<?php $frontendSelectQuery = 'select * from registered_event where event_status = 1 ORDER BY start_date';
$frontendSelectQueryResult = mysqli_query($conn, $frontendSelectQuery);
if (mysqli_num_rows($frontendSelectQueryResult) == 0) { ?>
	<div class="section group">
		<div class="grid_1_of_3 events_1_of_3">
			<div class="event-time">
				<h4>
					<span>sed quia non numqua</span>
				</h4>
				<h4>Sep 20Th, 2013</h4>
			</div>
			<div class="event-img">
				<a href="blog.php">
					<img src="images/event-img1.jpg" alt="">
					<span>Read More</span>
				</a>
			</div>
		</div>
		<div class="grid_1_of_3 events_1_of_3">
			<div class="event-time">
				<h4>
					<span>Sed ut perspiciatis</span>
				</h4>
				<h4>Sep 14TH, 2013</h4>
			</div>
			<div class="event-img">
				<a href="blog.php">
					<img src="images/event-img2.jpg" alt="">
					<span>Read More</span>
				</a>
			</div>
		</div>
		<div class="grid_1_of_3 events_1_of_3">
			<div class="event-time">
				<h4>
					<span> vel illum qui dolorem</span>
				</h4>
				<h4>Sep 5TH, 2013</h4>
			</div>
			<div class="event-img">
				<a href="blog.php">
					<img src="images/event-img3.jpg" alt="">
					<span>Read More</span>
				</a>
			</div>
		</div>
	</div>
<?php }else {
?> <div class="section group"> <?php
						$count = 1;
						while ($row = mysqli_fetch_assoc($frontendSelectQueryResult)) {
						if( $count % 4 == 0 ){
						?>
					</div><div class="section group">
						<?php
						}
						$image_name = $row['event_banner_image'];

						$file = '../site_backend/'.$image_name; // 'images/'.$file (physical path)

						if (!file_exists($file)) {
							$image_name = 'images/no_image_gal.jpg';
						}else{
							$image_name = '../site_backend/'.$image_name;
						}
						?>
						<div class="grid_1_of_3 events_1_of_3">
							<div class="event-time">
								<h4>
									<span><?php echo $row['name']; ?></span>
								</h4>
								<h4><?php echo $row['start_date']; ?></h4>
							</div>
							<div class="event-img">
								<a href="event_details.php?gal_status=0&&event_id=<?php echo $row['event_id'] ?>">
									<img src="<?php echo $image_name; ?>" alt="">
									<span>Read More</span>
								</a>
							</div>
						</div>
						<?php $count++;}
						}?>
				</div>
			</div>
		</div>
	</div>
<?php require_once('footer.php');?>