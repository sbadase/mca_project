<?php
require_once('header.php');
if(!isset($_SESSION['user_data'])){
    header('Location: login.php');
}

if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php
}

//admin_reply_to_customer_inquiries.php?event_id=0&approval_status=1&&parent_id=2&&customer_id=1

if(isset($_GET['event_id']) && $_GET['event_id'] ==0
    && isset($_GET['approval_status']) && $_GET['approval_status'] ==1
    && isset($_GET['parent_id']) && !empty($_GET['parent_id'])
    && isset($_GET['customer_id']) && !empty($_GET['customer_id'])
    && isset($_POST['inquiry_reply_submit'])
    && isset($_POST['content']) && !empty($_POST['content'])
){
    $content = addslashes($_POST['content']);


    $insertQuery = "INSERT INTO customer_feedback_or_inquiries (event_id, approval_status, feedback,parent_id,customer_id,date,is_admin)
VALUES ('" . $_GET['event_id'] . "', '" . $_GET['approval_status'] . "', '" . $content . "', '" . $_GET['parent_id']  . "', '" . $_GET['customer_id']  . "', '" . date("Y-m-d h:i:sa") . "',1)";

    mysqli_query($conn, $insertQuery);
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header('Location: '.$actual_link);
}
?>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http
      quiv="X-UA-Compatible" content="ie=edge">
<title>Carbon - Admin Template</title>
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/simple-line-icons/css/simple-line-icons.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/vendor/font-awesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="../site_backend/js_css_other_ui_files/backend_ui/css/styles.css">
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <div class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            General or Event Specific Feed Backs
                        </div>

                        <div class="card-body">


                            <?php
                            $parent_id= '';
                            $customer_id = '';
                            $customer_name_id = '';
                            $adminSelectQuery = 'select * from customer_feedback_or_inquiries where feedback_id ='.$_GET['feedback_id'];
                            $adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
                            if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>

                                    <td colspan="6" align="center">No Data Found</td>
                                </tr>
                            <?php }else {
                                while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
                                $parent_id = $row['feedback_id'];
                                $customer_id = $row['customer_id'];

                                    ?>
                                      <p class="form-control-plaintext"> FeedBack ID : <b><?php echo $row['feedback_id']?></b></p>
                                            <?php
                                            $checkAlreadyExistQuery='select * from frontend_user where status=1 and id ='.$row['customer_id'];
                                            $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                            if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                $customer_name_id = $row['customer_id'].'/'.$data['user_name'];
                                                    ?>
                                                    <p class="form-control-plaintext">Customer ID/Name : <b><?php echo $row['customer_id'].'/'.$data['user_name']?></b></p>
                                                    <?php
                                                } }  ?>

                                                <p class="form-control-plaintext">Inquiry For : <b>General(About Site)</b></p>

                                                        <p class="form-control-plaintext"><b>Inquiry Content :</b><br> <?php echo wordwrap($row['feedback'],150,"<br>\n")?></p>

                                <?php }
                            }
                            ?>
                            <p class="form-control-plaintext"><b>Recent Communications :</b><br> </p>
                            <?php
                            $checkAlreadyExistQuery='select * from customer_feedback_or_inquiries where event_id=0 and parent_id ='.$parent_id;
                            $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                            if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                    ?>
                                    <p class="form-control-plaintext"><b>Ownership : </b><?php if ($data['is_admin']==1){ echo 'ADMIN';}else{echo $customer_name_id;}?>&nbsp;&nbsp;&nbsp;&nbsp; <b>Date and Time : </b><?php echo $data['date']; ?> </p>
                                    <?php
                                    echo $data['feedback'] ;echo '<hr><br>';
                                }

                            }

                            ?>
                            <br><br> <b>Reply:</b> <br><br>

                            <form action="customers_inquiries_detail.php?event_id=0&approval_status=1&&parent_id=<?php echo $parent_id?>&&customer_id=<?php echo $customer_id ?>&&feedback_id=<?php echo $_GET['feedback_id'] ?>" method="post">
                                <div>
      <textarea cols="80" rows="10" id="content" name="content" >
        &lt;h1&gt;Article Title&lt;/h1&gt;
        &lt;p&gt;Here's some sample text&lt;/p&gt;
      </textarea>
                                    <script type="text/javascript">
                                        CKEDITOR.replace('content' );
                                    </script>
                                    <input type="submit" name="inquiry_reply_submit" value="Submit"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?>
