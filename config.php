<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
$hostName = "localhost";
$userName = "root";
$password = "";
$dbName = "mca_project";
// Create connection
$conn = mysqli_connect($hostName, $userName, $password, $dbName);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>