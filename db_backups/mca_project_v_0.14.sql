/*
SQLyog Community v11.52 (64 bit)
MySQL - 10.1.28-MariaDB : Database - mca_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `backend_user` */

CREATE TABLE `backend_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  PRIMARY KEY (`id`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `backend_user` */

insert  into `backend_user`(`id`,`user_name`,`password`,`status`,`email`) values (7,'sachinbadase1@gmail.com','e6e061838856bf47e1de730719fb2609',1,'sachinbadase1@gmail.com'),(8,'sachinbadase2@gmail.com','e6e061838856bf47e1de730719fb2609',1,'sachinbadase2@gmail.com');

/*Table structure for table `customer_feedback_or_inquiries` */

CREATE TABLE `customer_feedback_or_inquiries` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `approval_status` tinyint(1) DEFAULT NULL,
  `feedback` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `record_type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `customer_feedback_or_inquiries` */

insert  into `customer_feedback_or_inquiries`(`feedback_id`,`event_id`,`approval_status`,`feedback`,`parent_id`,`customer_id`,`date`,`is_admin`,`record_type`) values (1,5,1,'Feedback occurs when outputs of a system are routed back as inputs as part of a chain of cause-and-effect that forms a circuit or loop.[2] The system can then be said to feed back into itself. The notion of cause-and-effect has to be handled carefully when applied to feedback systems:\r\n\r\nSimple causal reasoning about a feedback system is difficult because the first system influences the second and second system influences the first, leading to a circular argument. This makes reasoning based upon cause and effect tricky, and it is necessary to analyze the system as a whole.',0,1,NULL,NULL,NULL),(2,0,1,'\r\n96\r\ndown vote\r\naccepted\r\n$_SERVER[\'HTTP_REFERER\'] will give you the referrer page\'s URL if there exists any. If users use a bookmark or directly visit your site by manually typing in the URL, http_referer will be empty. Also if the users are posting to your page programatically (CURL) then they\'re not obliged to set the http_referer as well. You\'re missing all _, is that a typo?',0,1,NULL,NULL,NULL),(3,0,1,'please find addtional details',2,1,NULL,NULL,NULL),(4,2147483647,0,'I love this site',2147483647,1,NULL,NULL,NULL),(13,0,1,'<h1>Article Title</h1>\r\n\r\n<p>Here&#39;s some sample text</p>\r\n',2,1,'2018-02-16 02:32:25pm',1,NULL),(14,0,1,'<h1>Article Title</h1>\r\n\r\n<p>Here&#39;s some sample text</p>\r\n',2,1,'2018-02-16 02:49:31pm',1,NULL),(15,0,1,'<p><span style=\"color:#e67e22\"><strong>thanks for reply</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n',2,1,'2018-02-16 02:50:14pm',1,NULL),(18,2,1,'<p><span style=\"color:#2ecc71\"><span style=\"font-size:72px\">Nice</span></span></p>\r\n',0,3,'2018-02-23 10:39:53am',0,2),(19,1,0,'<p><span style=\"color:#16a085\"><span style=\"font-size:48px\">need Ticket how i get it</span></span></p>\r\n',0,4,'2018-02-23 12:17:45pm',0,1),(20,1,0,'<h1>Article Title</h1>\r\n\r\n<p>Here&#39;s some sample text</p>\r\n',0,4,'2018-02-23 12:18:24pm',0,1),(21,1,0,'<h1>Article Title</h1>\r\n\r\n<p>Here&#39;s some sample text</p>\r\n',0,4,'2018-02-23 12:18:39pm',0,1),(22,1,0,'<h1>Article Title</h1>\r\n\r\n<p>Here&#39;s some sample text</p>\r\n',0,4,'2018-02-23 12:19:04pm',0,1),(23,2,0,'<h1>Article Title</h1>\r\n\r\n<p>Here&#39;s some sample text</p>\r\n',0,4,'2018-02-23 12:19:37pm',0,1),(24,9999999,0,'<p>i need your help</p>\r\n',0,4,'2018-02-23 12:27:09pm',0,1),(25,9999999,0,'<p><span style=\"color:#e67e22\"><span style=\"font-size:10px\">I need your help </span></span></p>\r\n',0,4,'2018-02-23 12:27:31pm',0,1),(26,0,1,'<p>sure can we contact you</p>\r\n',19,4,'2018-02-23 12:40:14pm',1,NULL),(27,0,1,'<p>9730779189</p>\r\n',19,4,'2018-02-23 12:42:48pm',1,NULL),(28,0,1,'<p>please reply soon</p>\r\n',19,4,'2018-02-23 12:44:30pm',1,NULL),(29,0,1,'<p>waiting for reply</p>\r\n',19,4,'2018-02-23 12:45:13pm',1,NULL),(30,0,1,'<p><span style=\"color:#2ecc71\"><span style=\"font-size:48px\">will call you today </span></span></p>\r\n',19,4,'2018-02-23 12:46:12pm',1,NULL),(31,1,0,'<p>site not working</p>\r\n',0,4,'2018-02-23 01:29:30pm',0,1),(32,1,0,'<p>site not working</p>\r\n',0,4,'2018-02-23 01:30:28pm',0,1),(33,1,0,'<p>site not working</p>\r\n',0,4,'2018-02-23 01:30:48pm',0,1),(34,0,1,'<p>Site under maintanance</p>\r\n',1,4,'2018-02-23 01:34:06pm',1,NULL),(35,0,1,'<p>gggdgddgdg</p>\r\n',1,1,'2018-02-23 01:37:37pm',1,NULL);

/*Table structure for table `employee_department` */

CREATE TABLE `employee_department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `employee_department` */

insert  into `employee_department`(`department_id`,`name`,`description`,`status`) values (1,'Accounts',' financial accounting</p>',0);

/*Table structure for table `employee_info` */

CREATE TABLE `employee_info` (
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_info` */

/*Table structure for table `event_category` */

CREATE TABLE `event_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `event_category` */

insert  into `event_category`(`category_id`,`name`,`description`,`status`) values (3,'Movies','qwer',0);

/*Table structure for table `event_tickets` */

CREATE TABLE `event_tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `ticket_code` text,
  `number_of_tickets` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `total_billed` int(11) DEFAULT NULL,
  `ticket_readable_string` text,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `event_tickets` */

insert  into `event_tickets`(`ticket_id`,`event_id`,`customer_id`,`ticket_code`,`number_of_tickets`,`unit_price`,`total_billed`,`ticket_readable_string`) values (17,1,3,'/Booked_Tickets/event_id_1/NM1978476.html',4,50000,200000,'NM1978476'),(18,1,3,'/Booked_Tickets/event_id_1/FF8614032.html',3,50000,150000,'FF8614032');

/*Table structure for table `frontend_user` */

CREATE TABLE `frontend_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `frontend_user` */

insert  into `frontend_user`(`id`,`user_name`,`password`,`status`,`phone_number`,`type`,`email`) values (1,'sachin',NULL,1,342443242,NULL,NULL),(2,'sachin.badase@perficient.com','e6e061838856bf47e1de730719fb2609',NULL,552532532520,'1',NULL),(3,'sachinbadase1@gmail.com','81dc9bdb52d04dc20036dbd8313ed055',1,9730779189,'2',NULL),(4,'sachinbadase2@gmail.com','e6e061838856bf47e1de730719fb2609',1,463646363,'2',NULL);

/*Table structure for table `image_gallery` */

CREATE TABLE `image_gallery` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `header_slider` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `image_gallery` */

insert  into `image_gallery`(`image_id`,`image_name`,`status`,`header_slider`) values (1,'1.jpg',1,1),(2,'2.jpg',1,1),(3,'3.jpg',1,1),(4,'4.jpg',1,1),(5,'5.jpg',1,1),(8,'MTI4NzdlNWI5N2I0ZDYxMDQ4ZTI4Y2E5NjZjOTBjNDUDesert.jpg',1,1),(9,'OTBkM2U0YTZjODgxM2M0NDIxNGUzZTUwOTQxNGMyNmYHydrangeas.jpg',1,1),(10,'NzdmMjkzNGE1NjI0MzY5NjllOGJmODMxZjUxNjUzODEJellyfish.jpg',1,1),(11,'ZTQwMGYwYTNiNTIxOTU3MTU5NTM5NDQ5NGJiNGZiNGQJellyfish.jpg',1,1),(12,'NjBlZTE5M2RmM2ViMTVmYjY4YzExNGM2ZjE1YzZhYjQChrysanthemum.jpg',1,0),(13,'ZmM3NjVkZDllYWQwNzAxMmM4NTk0ZmU1NmJjMmVlMDQKoala.jpg',1,0),(14,'MTk5MDI0ZTZiMzY3YzVjZjU2MDQ5MmIwMTgwOTIxNmIPenguins.jpg',1,0),(15,'YWYyMGEwNDFkNTQ1ZWY1YzgwYjcyNzQ2NGFjMDY5N2IPenguins.jpg',0,0),(16,'N2Y3ZGNjZDQ1ZDQ0YTdiMjVlMjY1N2M5ZGNlMWE4ZWYHydrangeas.jpg',1,0),(17,'YjY0ODUxMGNmYTBjNzU4ZTVlYWQyZTMzZjI0MWJmN2MTulips.jpg',1,0);

/*Table structure for table `registered_event` */

CREATE TABLE `registered_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `address` text,
  `start_date` varchar(256) DEFAULT NULL,
  `chief_guest` text,
  `end_date` varchar(256) DEFAULT NULL,
  `event_status` varchar(256) DEFAULT NULL,
  `event_type` int(11) DEFAULT NULL,
  `description` longtext,
  `event_banner_image` text,
  `price` int(11) DEFAULT NULL,
  `event_by` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `registered_event` */

insert  into `registered_event`(`event_id`,`name`,`address`,`start_date`,`chief_guest`,`end_date`,`event_status`,`event_type`,`description`,`event_banner_image`,`price`,`event_by`) values (1,'Himesh Reshmmiya Live in Concert new','fsdfsdfsdfsdf','2/22/2018 12:00 AM','fsfsdfsdfsdfmds','2/22/2018 12:00 AM','1',3,'dfsdfsfdsdfasf','event_uploads/NGUzYzEzZjA3MjUzM2IzYjYyMzkxYzcwYWFmNDE1OTcChrysanthemum.jpg',50000,'1'),(2,'Himesh Reshmmiya Live in Concert','ffasfasfasfasfas','2/22/2018 12:00 AM','Himesh Reshmmiya','2/22/2018 12:00 AM','1',3,'I was tearing my hair out trying to figure this the other morning and its really quite simple. I thought i may have use a day month year match or possibly use PHP but thankfully the guys at MySQL included this nice DATE() function which means you donâ€™t have to worry about the hours, minutes and seconds being different. Simples!\r\nMySQL DATE / TIME Functions\r\n\r\nThe functions used in thie MySQL query are:\r\n\r\n* DATE() returns the date without time\r\n* NOW() returns the current date & time (note weâ€™ve used the DATE() function in this query to remove the time)\r\n\r\nFor more information about MySQL Date and Time functions on the official MySQL site.','event_uploads/NGQzMWRiNTgzMjUxYzE4NmE2YzJiNWM3NzM4OTYzMmIHydrangeas.jpg',24,'1/sachin');

/*Table structure for table `support_ticket` */

CREATE TABLE `support_ticket` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `approval_status` tinyint(1) DEFAULT NULL,
  `feedback` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `record_type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `support_ticket` */

insert  into `support_ticket`(`feedback_id`,`event_id`,`approval_status`,`feedback`,`parent_id`,`customer_id`,`date`,`is_admin`,`record_type`) values (1,1,0,'<p>site not working</p>\r\n',0,4,'2018-02-23 01:31:41pm',0,1),(2,1,0,'<p>site not working</p>\r\n',0,4,'2018-02-23 01:32:20pm',0,1),(3,0,1,'<p>please reply</p>\r\n',1,4,'2018-02-23 01:40:41pm',1,NULL),(4,0,1,'<p>please reply</p>\r\n',1,4,'2018-02-23 01:41:35pm',NULL,NULL),(5,0,1,'<p>please check now</p>\r\n',1,4,'2018-02-23 01:51:53pm',1,NULL),(6,0,1,'<p>still issue</p>\r\n',1,4,'2018-02-23 01:52:48pm',NULL,NULL),(7,0,1,'<p>now fixed</p>\r\n',1,4,'2018-02-23 01:52:58pm',1,NULL);

/*Table structure for table `support_ticket_bkp` */

CREATE TABLE `support_ticket_bkp` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `issue_description` longtext,
  `ticket_status` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `support_ticket_bkp` */

/*Table structure for table `support_ticket_bkp_new` */

CREATE TABLE `support_ticket_bkp_new` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `feedback` longtext,
  `parent_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `current_status` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `support_ticket_bkp_new` */

insert  into `support_ticket_bkp_new`(`feedback_id`,`event_id`,`feedback`,`parent_id`,`customer_id`,`date`,`is_admin`,`current_status`) values (1,5,'send me your contact details',0,1,NULL,NULL,'open'),(2,5,'9730779189',1,1,NULL,1,'resolved'),(3,5,'\nArticle Title\nHeres some sample text\n',1,1,'2018-02-18 05:13:05am',1,'resolved'),(4,5,'\nArticle Title\nHeres some sample text\n',1,1,'2018-02-18 05:14:40am',1,'resolved'),(5,5,'<p><span style=\"font-size:48px\"><span style=\"color:#27ae60\">Ho gaya bhai ab</span></span></p>\r\n',1,1,'2018-02-18 05:16:32am',1,'resolved');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
