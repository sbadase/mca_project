/*
SQLyog Community v11.52 (64 bit)
MySQL - 10.1.28-MariaDB : Database - mca_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `backend_user` */

CREATE TABLE `backend_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  PRIMARY KEY (`id`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `backend_user` */

insert  into `backend_user`(`id`,`user_name`,`password`,`status`,`email`) values (7,'sachinbadase1@gmail.com','e6e061838856bf47e1de730719fb2609',1,'sachinbadase1@gmail.com'),(8,'sachinbadase2@gmail.com','e6e061838856bf47e1de730719fb2609',1,'sachinbadase2@gmail.com'),(9,'sachinbadase3@gmail.com','e6e061838856bf47e1de730719fb2609',0,'sachinbadase3@gmail.com');

/*Table structure for table `customer_feedback` */

CREATE TABLE `customer_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `approval_status` tinyint(1) DEFAULT NULL,
  `feedback` longtext,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer_feedback` */

/*Table structure for table `employee_department` */

CREATE TABLE `employee_department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_department` */

/*Table structure for table `employee_info` */

CREATE TABLE `employee_info` (
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_info` */

/*Table structure for table `event_category` */

CREATE TABLE `event_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `event_category` */

insert  into `event_category`(`category_id`,`name`,`description`,`status`) values (3,'Movies','qwer',0);

/*Table structure for table `event_tickets` */

CREATE TABLE `event_tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `ticket_code` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `event_tickets` */

/*Table structure for table `frontend_user` */

CREATE TABLE `frontend_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `adharcard_number` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `frontend_user` */

/*Table structure for table `registered_event` */

CREATE TABLE `registered_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `address` text,
  `start_date` varchar(256) DEFAULT NULL,
  `chief_guest` text,
  `end_date` varchar(256) DEFAULT NULL,
  `event_status` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `registered_event` */

/*Table structure for table `support_ticket` */

CREATE TABLE `support_ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `issue_description` longtext,
  `ticket_status` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `support_ticket` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
