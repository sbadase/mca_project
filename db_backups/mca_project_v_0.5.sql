/*
SQLyog Community v11.52 (64 bit)
MySQL - 10.1.28-MariaDB : Database - mca_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `backend_user` */

CREATE TABLE `backend_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  PRIMARY KEY (`id`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `backend_user` */

insert  into `backend_user`(`id`,`user_name`,`password`,`status`,`email`) values (7,'sachinbadase1@gmail.com','e6e061838856bf47e1de730719fb2609',1,'sachinbadase1@gmail.com'),(8,'sachinbadase2@gmail.com','e6e061838856bf47e1de730719fb2609',1,'sachinbadase2@gmail.com'),(9,'sachinbadase3@gmail.com','e6e061838856bf47e1de730719fb2609',0,'sachinbadase3@gmail.com');

/*Table structure for table `customer_feedback` */

CREATE TABLE `customer_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `approval_status` tinyint(1) DEFAULT NULL,
  `feedback` longtext,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer_feedback` */

/*Table structure for table `employee_department` */

CREATE TABLE `employee_department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `employee_department` */

insert  into `employee_department`(`department_id`,`name`,`description`,`status`) values (1,'Accounts',' financial accounting</p>',0);

/*Table structure for table `employee_info` */

CREATE TABLE `employee_info` (
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_info` */

/*Table structure for table `event_category` */

CREATE TABLE `event_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `event_category` */

insert  into `event_category`(`category_id`,`name`,`description`,`status`) values (3,'Movies','qwer',0);

/*Table structure for table `event_tickets` */

CREATE TABLE `event_tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `ticket_code` text,
  `number_of_tickets` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `total_billed` int(11) DEFAULT NULL,
  `ticket_readable_string` text,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `event_tickets` */

insert  into `event_tickets`(`ticket_id`,`event_id`,`customer_id`,`ticket_code`,`number_of_tickets`,`unit_price`,`total_billed`,`ticket_readable_string`) values (1,9,1,'Booked_Tickets/event_id_9//GX1081686.html',1,21,0,NULL),(2,7,1,'Booked_Tickets/event_id_7//RB1511278.html',2,70,140,NULL),(3,5,2147483647,'Booked_Tickets/event_id_5/GX8638565.html',3,20,60,NULL),(4,5,1,'/Booked_Tickets/event_id_5/BU3414799.html',4,20,80,'BU3414799'),(5,7,1,'/Booked_Tickets/event_id_7/HS0488116.html',4,70,280,'HS0488116'),(6,7,1,'/Booked_Tickets/event_id_7/MT2847956.html',1,70,0,'MT2847956'),(7,7,1,'/Booked_Tickets/event_id_7/PJ1786189.html',3,70,210,'PJ1786189');

/*Table structure for table `frontend_user` */

CREATE TABLE `frontend_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `adharcard_number` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `frontend_user` */

insert  into `frontend_user`(`id`,`user_name`,`password`,`status`,`phone_number`,`adharcard_number`,`type`) values (1,'sachin',NULL,1,342443242,434324234324324,NULL);

/*Table structure for table `registered_event` */

CREATE TABLE `registered_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `address` text,
  `start_date` varchar(256) DEFAULT NULL,
  `chief_guest` text,
  `end_date` varchar(256) DEFAULT NULL,
  `event_status` varchar(256) DEFAULT NULL,
  `event_type` int(11) DEFAULT NULL,
  `description` longtext,
  `event_banner_image` text,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `registered_event` */

insert  into `registered_event`(`event_id`,`name`,`address`,`start_date`,`chief_guest`,`end_date`,`event_status`,`event_type`,`description`,`event_banner_image`,`price`) values (5,'Padmavati','Poonam chembers nagpur','2/4/2017 12:00 AM','Deepika ranvir sachin amit  test qwer','3/23/2017 12:00 AM','1',3,'Nagpur Event','event_uploads/M2Q0ODBhZjRkNGEyNjdmODIzOTM0MTg1YzQ2OGYzMzU2.png',20),(6,'race 3','324323424','2/15/2018 12:00 AM','e423131323','2/15/2018 12:00 AM','0',0,'43422424','event_uploads/1.png',40),(7,'dgdgdgdg','43441','2/15/2018 12:00 AM','2444411','2/15/2018 12:00 AM','1',3,'323414','event_uploads/71.png',70),(8,'erererwr','42342424','2/15/2018 12:00 AM','sfsfsfsfsfs','2/15/2018 12:00 AM','0',0,'erererere','event_uploads/OWRiMDdlOTk4NzYyZDJlZDE1NWEzOTcwODIxOTI5ZTg1.png',87),(9,'mynewevent','rfsfsfsfv','2/15/2018 12:00 AM','45552552','2/15/2018 12:00 AM','1',0,'55252352','event_uploads/NzkwZmFmZDQxN2U5Mzg1MTk3NDMyOWNmOWJkZDRkMDI1.png',21);

/*Table structure for table `support_ticket` */

CREATE TABLE `support_ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `issue_description` longtext,
  `ticket_status` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `support_ticket` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
