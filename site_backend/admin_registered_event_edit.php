<style>
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 150px;
    }

    img:hover {
        box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
    }
</style>
<?php


require_once('header.php');

if(isset($_GET['id'])
    && !empty($_GET['id'])
    && is_numeric($_GET['id'])){
    $selectedAccountCheckQuery = 'select *  from registered_event where event_id= '.$_GET['id'];
    $selectedAccountCheckQueryResult = mysqli_query($conn, $selectedAccountCheckQuery);
    $data = mysqli_fetch_assoc($selectedAccountCheckQueryResult);
    $registered_event_name = $data['name'];
    $registered_event_address = $data['address'];
    $registered_event_start_date = $data['start_date'];
    $registered_event_chief_guest = $data['chief_guest'];
    $registered_event_end_date= $data['end_date'];
    $registered_event_status= $data['event_status'];
    $registered_event_type= $data['event_type'];
    $registered_event_description= $data['description'];
    $registered_event_end_event_banner_image= $data['event_banner_image'];
    $registered_event_price= $data['price'];
}
else{
    header('Location: admin_registered_event_grid.php');

}
if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php

}
if(isset($_POST['admin_edit_registered_event_submit'])){

    $error=0;

    if(empty($_POST['registered_event_address'])){
        $error=1;
    }
    if(empty($_POST['registered_event_description'])){

        $error=1;
    }
    if(empty($_POST['registered_event_chief_guest'])){

        $error=1;
    }
    if(empty($_POST['registered_event_start_date'])){
        $error=1;
    }
    if(empty($_POST['registered_event_end_date'])){

        $error=1;
    }
    if(is_null($_POST['registered_event_status'])){

        $error=1;
    }
    if(is_null($_POST['registered_event_type'])){

        $error=1;
    }

    if($error == 0){

        if(isset($_FILES['file'])){
            if($_FILES['file']['error'] > 0) {
                echo 'Error during uploading, try again'; }
            $extsAllowed = array( 'jpg', 'jpeg', 'png', 'gif' );
             $extUpload = strtolower( substr( strrchr($_FILES['file']['name'], '.') ,1) ) ;
             if (in_array($extUpload, $extsAllowed) ) {
                 unlink('./'.$_POST['old_img_name']);

                $newFileName=  rtrim(base64_encode(md5(microtime())),"=").$_FILES['file']['name'];
                $name = "event_uploads/{$newFileName}";
                $result = move_uploaded_file($_FILES['file']['tmp_name'], $name);
                if($result){echo "<img src='$name'/>";}
            } else {         ?>
                <script>
                    alert("Invalid File Upload Event,Event Not Updated,Please Try again");
                </script>
                <?php
                 $updateQuery = "UPDATE `registered_event` SET `address`='" . $_POST['registered_event_address'] . "',`start_date`='" . $_POST['registered_event_start_date'] . "',`chief_guest`='" . $_POST['registered_event_chief_guest'] . "',`end_date`='" . $_POST['registered_event_end_date'] . "',`event_status`='" . $_POST['registered_event_status'] . "',`event_type`='" . $_POST['registered_event_type'] . "',`description`='" . $_POST['registered_event_description'] . "' , `price`='" . $_POST['registered_event_price'] . "' where event_id =".$_GET['id'];
                 mysqli_query($conn, $updateQuery);
                 header('Location: admin_registered_event_edit.php?id='.$_GET['id'].'&&msg=Invalid File Upload Event,Event Not Registered,Please Try again');
                exit;
            }
        }else{

            $updateQuery = "UPDATE `registered_event` SET `address`='" . $_POST['registered_event_address'] . "',`start_date`='" . $_POST['registered_event_start_date'] . "',`chief_guest`='" . $_POST['registered_event_chief_guest'] . "',`end_date`='" . $_POST['registered_event_end_date'] . "',`event_status`='" . $_POST['registered_event_status'] . "',`event_type`='" . $_POST['registered_event_type'] . "',`description`='" . $_POST['registered_event_description'] . "'  , `price`='" . $_POST['registered_event_price'] . "' where event_id =".$_GET['id'];
            mysqli_query($conn, $updateQuery);
            header('Location: admin_registered_event_grid.php');
        }

        if(!empty($name)){
             $updateQuery = "UPDATE `registered_event` SET `address`='" . $_POST['registered_event_address'] . "',`start_date`='" . $_POST['registered_event_start_date'] . "',`chief_guest`='" . $_POST['registered_event_chief_guest'] . "',`end_date`='" . $_POST['registered_event_end_date'] . "',`event_status`='" . $_POST['registered_event_status'] . "',`event_type`='" . $_POST['registered_event_type'] . "',`description`='" . $_POST['registered_event_description'] . "',`event_banner_image`='" . $name . "'  , `price`='" . $_POST['registered_event_price'] . "' where event_id =".$_GET['id'];
            mysqli_query($conn, $updateQuery);
            header('Location: admin_registered_event_grid.php');
        }else{

             $updateQuery = "UPDATE `registered_event` SET `address`='" . $_POST['registered_event_address'] . "',`start_date`='" . $_POST['registered_event_start_date'] . "',`chief_guest`='" . $_POST['registered_event_chief_guest'] . "',`end_date`='" . $_POST['registered_event_end_date'] . "',`event_status`='" . $_POST['registered_event_status'] . "',`event_type`='" . $_POST['registered_event_type'] . "',`description`='" . $_POST['registered_event_description'] . "'   , `price`='" . $_POST['registered_event_price'] . "' where event_id =".$_GET['id'];
            mysqli_query($conn, $updateQuery);
            header('Location: admin_registered_event_grid.php');
        }

    }else{

        ?>
        <script>
            alert("Please Fill Compulsory Fields ");
        </script>
        <?php
    }


}
?>



<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                Edit Register Event
                            </div>
                            <form method="post" action="admin_registered_event_edit.php?id=<?php echo $_GET['id']?>" class="form-horizontal" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Event Name</label>
                                                <p class="form-control-plaintext"><?php echo $registered_event_name?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event Ticket Price(Per Person)</label>
                                                <input type="number" value="<?php echo $registered_event_price;?>" id="normal-input" name="registered_event_price" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Pick a file :</label>
                                                <a target="_blank" href="<?php echo $registered_event_end_event_banner_image;?>">
                                                    <img src="<?php echo $registered_event_end_event_banner_image;?>" alt="<?php echo $registered_event_end_event_banner_image;?>" style="width:150px">
                                                </a>
                                                <input type="hidden" name="old_img_name" value="<?php echo $registered_event_end_event_banner_image;?>">
                                                <input type="file" id="normal-input" name ="file">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event Address</label>
                                                <textarea id="normal-input"  name="registered_event_address" class="form-control" required><?php echo $registered_event_address;?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event Description</label>
                                                <textarea id="normal-input"  name="registered_event_description" class="form-control" required><?php echo $registered_event_description;?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event Chief Guest</label>
                                                <textarea id="normal-input"  name="registered_event_chief_guest" class="form-control" required><?php echo $registered_event_chief_guest;?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event Start Date and Time</label>
                                                <input id="start" name="registered_event_start_date" value="<?php echo $registered_event_start_date;?>" title="datetimepicker" style="width: 100%;" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event End Date and Time</label>
                                                <input id="end" value="<?php echo $registered_event_end_date;?>" name="registered_event_end_date" title="datetimepicker" style="width: 100%;" />
                                            </div>
                                        </div>
                                        <div class="col md-4">
                                            <div class="form-group">
                                                <label for="single-select">Event Type</label>
                                                <select id="single-select" name="registered_event_type" class="form-control" required>
                                                    <option value="0">Any</option>
                                                    <?php
                                                    $checkAlreadyExistQuery='select * from event_category ';
                                                    $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                    if (mysqli_num_rows($checkAlreadyExistQueryResult)> 0) {
                                                        while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                            ?>
                                                            <option <?php if ($registered_event_type == $data['category_id'] ){ echo 'selected';}?> value="<?php echo $data['category_id']; ?>"><?php echo $data['name']; ?></option>
                                                            <?php
                                                        } }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col md-4">
                                            <div class="form-group">
                                                <label for="single-select">Event Category Status</label>
                                                <select id="single-select" name="registered_event_status" class="form-control" required>
                                                    <option value="0" <?php if($registered_event_status==0){ echo "selected";}?> >InActive</option>
                                                    <option value="1" <?php if($registered_event_status==1){ echo "selected";}?>>Active</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input type="submit" name="admin_edit_registered_event_submit" value="Update Register Event">
									<button><td><a href="admin_registered_event_grid.php">cancel</a></td></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?>
<style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
<title></title>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.material.mobile.min.css" />

<script src="https://kendo.cdn.telerik.com/2018.1.117/js/jquery.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.117/js/kendo.all.min.js"></script>
<script>
    $(document).ready(function() {
        function startChange() {
            var startDate = start.value(),
                endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.min(new Date(endDate));
            } else {
                endDate = new Date();
                start.min(endDate);
                end.min(endDate);
            }
        }

        function endChange() {
            var endDate = end.value(),
                startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.min(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.min(endDate);
                end.min(endDate);
            }
        }

        var today = kendo.date.today();

        var start = $("#start").kendoDateTimePicker({
            value: today,
            change: startChange,
            parseFormats: ["YYYY-MM-DD HH:MM:SS"]
        }).data("kendoDateTimePicker");

        var end = $("#end").kendoDateTimePicker({
            value: today,
            change: endChange,
            parseFormats: ["YYYY-MM-DD HH:MM:SS"]
        }).data("kendoDateTimePicker");

        start.min(end.value());
        end.min(start.value());
        $('#start').val('<?php echo $registered_event_start_date ?>');
        $('#end').val('<?php echo $registered_event_end_date ?>');
    });
</script>
</body>

</html>
