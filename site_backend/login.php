<?php require_once('../config.php');
//$userName= 'sachin';
//$passWord= '123';
$userVerificationStatus=0;
if(isset($_POST['user_verify'])){

    $error=0;
    if(empty($_POST['admin_email'])){
        $error=1;
    }
    if(empty($_POST['admin_password'])){
        $error=1;
    }

    if($error == 0 ){

        $adminSelectedAccountCheckQuery = 'select *  from backend_user where user_name = "'.$_POST['admin_email'].'" and password ="'.md5($_POST['admin_password']).'" and status = 1';
        $adminSelectedAccountCheckQueryResult = mysqli_query($conn, $adminSelectedAccountCheckQuery);

    }
if (mysqli_num_rows($adminSelectedAccountCheckQueryResult) == 1) {
    $userVerificationStatus = 1;
    $data = mysqli_fetch_assoc($adminSelectedAccountCheckQueryResult);
    $userName = $data['email'];
}else{

    ?>
    <script>
        alert("Invalid User ");
    </script>
    <?php
}
}

    if($userVerificationStatus == 1)
    {
        $_SESSION['user_name']= $userName;
        $_SESSION['login_verify']= $userVerificationStatus;
        header('Location: index.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carbon - Admin Template</title>
    <link rel="stylesheet" href="./js_css_other_ui_files/backend_ui/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="./js_css_other_ui_files/backend_ui/vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="./js_css_other_ui_files/backend_ui/css/styles.css">
</head>
<body><body>
<div class="page-wrapper flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card p-4">
                    <div class="card-header text-center text-uppercase h4 font-weight-light">
                        Login
                    </div>
                    <form method="post" action="login.php">
                    <div class="card-body py-5">
                        <div class="form-group">
                            <label class="form-control-label">Email</label>
                            <input type="email" name="admin_email" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label class="form-control-label">Password</label>
                            <input type="password" name="admin_password" class="form-control" required>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <button type="submit" name="user_verify" class="btn btn-primary px-5">Login</button>
                            </div>
</form>
                            <div class="col-6">
                                <a href="#" class="btn btn-link">Forgot password?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
