        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-title">Navigation</li>

                    <li class="nav-item">
                        <a href="http://localhost/mca_project/site_backend/index.php" class="nav-link active">
                            <i class="icon icon-speedometer"></i> Dashboard
                        </a>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Admin Accounts <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_account_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View All
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_add_new_form.php" class="nav-link">
                                    <i class="icon icon-target"></i> Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Event Category<i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_event_category_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View All
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_event_category_add_new_form.php" class="nav-link">
                                    <i class="icon icon-target"></i> Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Employee Department<i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_employee_department_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View All
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_employee_department_add_new_form.php" class="nav-link">
                                    <i class="icon icon-target"></i> Add New
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i>Event's Management<i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_registered_event_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View All
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_registered_event_add_new_form.php" class="nav-link">
                                    <i class="icon icon-target"></i> Registered/Add New Event
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Event Tickets<i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_event_tickets_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View Booked Tickets
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_add_new_event_tickets.php" class="nav-link">
                                    <i class="icon icon-target"></i> Book Ticket
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Customer FeedBack<i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_customer_feedback_or_inquiries_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View Feed Backs
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Customer Inquiries <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_customer_inquiries_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View Customer Inquiries
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Image/Gallery/Header Slider Management  <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_image_gallery_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> Show Images
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_image_gallery_upload.php" class="nav-link">
                                    <i class="icon icon-target"></i> Upload Image
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-target"></i> Support_Tickets <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/admin_support_ticket_grid.php" class="nav-link">
                                    <i class="icon icon-target"></i> View Tickets
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-energy"></i> UI Kits <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/alerts.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Alerts
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/buttons.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Buttons
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/cards.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Cards
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/modals.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Modals
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/tabs.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Tabs
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/progress-bars.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Progress Bars
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/widgets.php" class="nav-link">
                                    <i class="icon icon-energy"></i> Widgets
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-graph"></i> Charts <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/chartjs.php" class="nav-link">
                                    <i class="icon icon-graph"></i> Chart.js
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="http://localhost/mca_project/site_backend/forms.php" class="nav-link">
                            <i class="icon icon-puzzle"></i> Forms
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="http://localhost/mca_project/site_backend/tables.php" class="nav-link">
                            <i class="icon icon-grid"></i> Tables
                        </a>
                    </li>

                    <li class="nav-title">More</li>

                    <li class="nav-item nav-dropdown">
                        <a href="http://localhost/mca_project/site_backend/#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-umbrella"></i> Pages <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/blank.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> Blank Page
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/login.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> Login
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/register.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> Register
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/invoice.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> Invoice
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/404.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> 404
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/500.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> 500
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="http://localhost/mca_project/site_backend/settings.php" class="nav-link">
                                    <i class="icon icon-umbrella"></i> Settings
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>