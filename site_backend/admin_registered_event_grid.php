<?php require_once('header.php'); ?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Registered Events
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Start Date and Time</th>
                                        <th>End Date and Time</th>
                                        <th>Chief Guest</th>
                                        <th>Event Status</th>
                                        <th>Event Type</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $adminSelectQuery = 'select * from registered_event';
                                    $adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
                                    if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
                                        <tr>
                                            <td colspan="10" align="center">No Data Found</td>
                                        </tr>
                                    <?php }else {
                                        while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
                                            ?>
                                            <tr>
                                                <td> <a href="admin_registered_event_edit.php?id=<?php echo $row['event_id']?>" class="nav-link">
                                                        <i class="icon icon-target"></i> Edit
                                                    </a></td>
                                                <td> <a href="admin_registered_event_delete.php?id=<?php echo $row['event_id']?>" onclick="return confirm('Are you sure you want to delete this Registered Events?');" class="nav-link">
                                                        <i class="icon icon-target"></i> Delete
                                                    </a></td>
                                                <td><?php echo $row['event_id']?></td>
                                                <td class="text-nowrap"><?php echo $row['name']?></td>
                                                <td><?php echo wordwrap($row['address'],15,"<br>\n")?></td>
                                                <td><?php echo $row['start_date'] ?></td>
                                                <td><?php echo $row['end_date'] ?></td>
                                                <td><?php echo wordwrap($row['chief_guest'],15,"<br>\n")?></td>
                                                <td><?php  if($row['event_status'] == 0){ echo 'InActive';}else{ echo 'Active';}?></td>
                                                <td><?php
                              $checkAlreadyExistQuery='select name from event_category where category_id='.$row['event_type'];
                                                    $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                    if (mysqli_num_rows($checkAlreadyExistQueryResult) == 1) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                     echo $data['name'];
                                                         ;
                                                    }}
                                                    if($row['event_type'] == 0){
                                                    echo 'Any/All';
                                                    }
                                                    ?>

                                                </td>

                                            </tr>
                                        <?php }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
