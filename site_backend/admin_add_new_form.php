<?php require_once('header.php');

if(isset($_POST['admin_add_new_submit'])){
$error=0;
if(empty($_POST['admin_email'])){
    $error=1;
}
if(empty($_POST['admin_password'])){
    $error=1;
}
if($error == 0){
 $checkIfUserAlreadyExistQuery='select * from backend_user where email = "'.$_POST['admin_email'].'"';
    $checkIfUserAlreadyExistQueryResult=mysqli_query($conn, $checkIfUserAlreadyExistQuery);
    if (mysqli_num_rows($checkIfUserAlreadyExistQueryResult) == 1) {
        ?>
        <script>
            alert("user Already Exist");
        </script>
    <?php
    }else {
        $adminInsertQuery = "INSERT INTO backend_user (user_name, password, status,email)
VALUES ('" . $_POST['admin_email'] . "', '" . md5($_POST['admin_password']) . "', '" . $_POST['account_status'] . "','" . $_POST['admin_email'] . "')";
        mysqli_query($conn, $adminInsertQuery);
        header('Location: admin_account_grid.php');
    }
}
}
?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Add New Admin
                            </div>
<form method="post" action="admin_add_new_form.php" class="form-horizontal">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Admin Email</label>
                                            <input type="email" id="normal-input" name="admin_email" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Admin Password</label>
                                            <input id="normal-input" name="admin_password" class="form-control"  required>
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Account Status</label>
                                            <select id="single-select" name="account_status" class="form-control" required>
                                                <option value="0">InActive</option>
                                                <option value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" name="admin_add_new_submit" value="Add New Admin">
								<button><td><a href="index.php">cancel</a></td></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
