<?php require_once('header.php'); ?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <div class="page-header">
        <nav class="navbar page-header">
            <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto">
                <i class="fa fa-bars"></i>
            </a>

            <a class="navbar-brand" href="#">
                <img src="./imgs/logo.png" alt="logo">
            </a>

            <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
                <i class="fa fa-bars"></i>
            </a>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item d-md-down-none">
                    <a href="#">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-pill badge-danger">5</span>
                    </a>
                </li>

                <li class="nav-item d-md-down-none">
                    <a href="#">
                        <i class="fa fa-envelope-open"></i>
                        <span class="badge badge-pill badge-danger">5</span>
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="./imgs/avatar-1.png" class="avatar avatar-sm" alt="logo">
                        <span class="small ml-1 d-md-down-none">John Smith</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header">Account</div>

                        <a href="#" class="dropdown-item">
                            <i class="fa fa-user"></i> Profile
                        </a>

                        <a href="#" class="dropdown-item">
                            <i class="fa fa-envelope"></i> Messages
                        </a>

                        <div class="dropdown-header">Settings</div>

                        <a href="#" class="dropdown-item">
                            <i class="fa fa-bell"></i> Notifications
                        </a>

                        <a href="#" class="dropdown-item">
                            <i class="fa fa-wrench"></i> Settings
                        </a>

                        <a href="#" class="dropdown-item">
                            <i class="fa fa-lock"></i> Logout
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
    </div>

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>

        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header bg-light">
                            Normal Table
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Sales</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="text-nowrap">Samsung Galaxy S8</td>
                                        <td>31,589</td>
                                        <td>$800</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td class="text-nowrap">Google Pixel XL</td>
                                        <td>99,542</td>
                                        <td>$750</td>
                                        <td>3%</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td class="text-nowrap">iPhone X</td>
                                        <td>62,220</td>
                                        <td>$1,200</td>
                                        <td>0%</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td class="text-nowrap">OnePlus 5T</td>
                                        <td>50,000</td>
                                        <td>$650</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td class="text-nowrap">Google Nexus 6</td>
                                        <td>400</td>
                                        <td>$400</td>
                                        <td>7%</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header bg-light">
                            Striped Rows
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Sales</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="text-nowrap">Samsung Galaxy S8</td>
                                        <td>31,589</td>
                                        <td>$800</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td class="text-nowrap">Google Pixel XL</td>
                                        <td>99,542</td>
                                        <td>$750</td>
                                        <td>3%</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td class="text-nowrap">iPhone X</td>
                                        <td>62,220</td>
                                        <td>$1,200</td>
                                        <td>0%</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td class="text-nowrap">OnePlus 5T</td>
                                        <td>50,000</td>
                                        <td>$650</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td class="text-nowrap">Google Nexus 6</td>
                                        <td>400</td>
                                        <td>$400</td>
                                        <td>7%</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header bg-light">
                            Bordered Table
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Sales</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="text-nowrap">Samsung Galaxy S8</td>
                                        <td>31,589</td>
                                        <td>$800</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td class="text-nowrap">Google Pixel XL</td>
                                        <td>99,542</td>
                                        <td>$750</td>
                                        <td>3%</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td class="text-nowrap">iPhone X</td>
                                        <td>62,220</td>
                                        <td>$1,200</td>
                                        <td>0%</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td class="text-nowrap">OnePlus 5T</td>
                                        <td>50,000</td>
                                        <td>$650</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td class="text-nowrap">Google Nexus 6</td>
                                        <td>400</td>
                                        <td>$400</td>
                                        <td>7%</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header bg-light">
                            Hoverable Table
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Sales</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="text-nowrap">Samsung Galaxy S8</td>
                                        <td>31,589</td>
                                        <td>$800</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td class="text-nowrap">Google Pixel XL</td>
                                        <td>99,542</td>
                                        <td>$750</td>
                                        <td>3%</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td class="text-nowrap">iPhone X</td>
                                        <td>62,220</td>
                                        <td>$1,200</td>
                                        <td>0%</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td class="text-nowrap">OnePlus 5T</td>
                                        <td>50,000</td>
                                        <td>$650</td>
                                        <td>5%</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td class="text-nowrap">Google Nexus 6</td>
                                        <td>400</td>
                                        <td>$400</td>
                                        <td>7%</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
