<?php require_once('header.php');
if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php

}
if(isset($_POST['admin_add_new_registered_event_submit'])){

$error=0;
if(empty($_POST['registered_event_name'])){
    $error=1;
}
if(empty($_POST['registered_event_address'])){
    $error=1;
}
    if(empty($_POST['registered_event_description'])){
        $error=1;
    }
    if(empty($_POST['registered_event_chief_guest'])){
        $error=1;
    }
    if(empty($_POST['registered_event_start_date'])){
        $error=1;
    }
    if(empty($_POST['registered_event_end_date'])){
        $error=1;
    }
    if(is_null($_POST['registered_event_status'])){
        $error=1;
    }
    if(is_null($_POST['registered_event_type'])){
        $error=1;
    }
    if(is_null($_POST['registered_event_price'])){
        $error=1;
    }

if($error == 0){
 $checkAlreadyExistQuery='select * from registered_event where name = "'.$_POST['registered_event_name'].'"';
    $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
    if (mysqli_num_rows($checkAlreadyExistQueryResult) == 1) {
        ?>
        <script>
            alert("Event Already Registered");
        </script>
    <?php
    }else {

        if(isset($_FILES['file'])){
            if($_FILES['file']['error'] > 0) { echo 'Error during uploading, try again'; }

//We won't use $_FILES['file']['type'] to check the file extension for security purpose

//Set up valid image extensions
            $extsAllowed = array( 'jpg', 'jpeg', 'png', 'gif' );

//Extract extention from uploaded file
//substr return ".jpg"
//Strrchr return "jpg"

            $extUpload = strtolower( substr( strrchr($_FILES['file']['name'], '.') ,1) ) ;
//Check if the uploaded file extension is allowed

            if (in_array($extUpload, $extsAllowed) ) {

                //Upload the file on the server
              $newFileName=  rtrim(base64_encode(md5(microtime())),"=").$_FILES['file']['name'];
                $name = "event_uploads/{$newFileName}";
                $result = move_uploaded_file($_FILES['file']['tmp_name'], $name);
                if($result){echo "<img src='$name'/>";}

            } else {         ?>
                <script>
                    alert("Invalid File Upload Event,Event Not Registered,Please Try again");
                </script>
                <?php
                header('Location: admin_registered_event_add_new_form.php?msg=Invalid File Upload Event,Event Not Registered,Please Try again');
                exit;
            }


        }
        $insertQuery = "INSERT INTO registered_event (name,address,start_date,chief_guest,end_date,event_status,event_type,description,event_banner_image,price,event_by)
VALUES ('" . $_POST['registered_event_name'] . "', '" . $_POST['registered_event_address'] . "', '" . $_POST['registered_event_start_date'] . "', '" . $_POST['registered_event_chief_guest'] . "', '" . $_POST['registered_event_end_date'] . "', '" . $_POST['registered_event_status'] . "', '" . $_POST['registered_event_type'] . "', '" . $_POST['registered_event_description'] . "', '" . $name . "','" . $_POST['registered_event_price'] . "','" . $_POST['event_tickets_customer_id'] . "')";

        mysqli_query($conn, $insertQuery);
        header('Location: admin_registered_event_grid.php');
    }
}
}
?>



<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Add/Register New Event
                            </div>
<form method="post" action="admin_registered_event_add_new_form.php" class="form-horizontal" enctype="multipart/form-data">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event Name</label>
                                            <input type="text" id="normal-input" name="registered_event_name" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event Ticket Price(Per Person)</label>
                                            <input type="number" id="normal-input" name="registered_event_price" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Pick a file :</label>
                                            <input type="file" id="normal-input" name ="file">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event Address</label>
                                            <textarea id="normal-input"  name="registered_event_address" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event Description</label>
                                            <textarea id="normal-input"  name="registered_event_description" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event Chief Guest</label>
                                            <textarea id="normal-input"  name="registered_event_chief_guest" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event Start Date and Time</label>
                                            <input id="start" name="registered_event_start_date" title="datetimepicker" style="width: 100%;" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Event End Date and Time</label>
                                            <input id="end" name="registered_event_end_date" title="datetimepicker" style="width: 100%;" />
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Event Type</label>
                                            <select id="single-select" name="registered_event_type" class="form-control" required>
                                                <option value="0">Any</option>
                                                <?php
                                                $checkAlreadyExistQuery='select * from event_category ';
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) == 1) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <option value="<?php echo $data['category_id']; ?>"><?php echo $data['name']; ?></option>
                                                        <?php
                                                    } }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Event Added By Admin/Customer</label>
                                            <select id="single-select" name="event_tickets_customer_id" class="form-control" required>
                                                <option value="99999999999999">Admin</option>
                                                <?php
                                                $checkAlreadyExistQuery='select * from frontend_user where status=1';
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <option  value="<?php echo $data['id']; ?>/<?php echo $data['user_name  ']; ?>"><?php echo $data['user_name']; ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Event Category Status</label>
                                            <select id="single-select" name="registered_event_status" class="form-control" required>
                                                <option value="0">InActive</option>
                                                <option value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" name="admin_add_new_registered_event_submit" value="Register New Event">
								<button><td><a href="admin_registered_event_grid.php">cancel</a></td></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?>
<style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
<title></title>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.material.mobile.min.css" />

<script src="https://kendo.cdn.telerik.com/2018.1.117/js/jquery.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.117/js/kendo.all.min.js"></script>
<script>
    $(document).ready(function() {
        function startChange() {
            var startDate = start.value(),
                endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.min(new Date(endDate));
            } else {
                endDate = new Date();
                start.min(endDate);
                end.min(endDate);
            }
        }

        function endChange() {
            var endDate = end.value(),
                startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.min(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.min(endDate);
                end.min(endDate);
            }
        }

        var today = kendo.date.today();

        var start = $("#start").kendoDateTimePicker({
            value: today,
            change: startChange,
            parseFormats: ["YYYY-MM-DD HH:MM:SS"]
        }).data("kendoDateTimePicker");

        var end = $("#end").kendoDateTimePicker({
            value: today,
            change: endChange,
            parseFormats: ["YYYY-MM-DD HH:MM:SS"]
        }).data("kendoDateTimePicker");

        start.min(end.value());
        end.min(start.value());
    });
</script>
</body>

</html>
