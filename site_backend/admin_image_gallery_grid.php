<?php require_once('header.php'); ?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Image/Gallery/Header Slider Grid
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th> Image ID</th>
                                        <th>Image Name</th>
                                        <th>ThumbNail</th>
                                        <th>Current Status</th>
                                        <th>Included In Header Gallery</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
<?php $adminSelectQuery = 'select * from image_gallery';
$adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
    <tr>
        <td colspan="6" align="center">No Data Found</td>
    </tr>
<?php }else {
    while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
        ?>
        <tr>
            <td><?php echo $row['image_id']?></td>
            <td><?php echo $row['image_name']?></td>
            <td><?php  $image_name = $row['image_name'];
                $file = '../site_frontend/images/'.$image_name; // 'images/'.$file (physical path)

                if (!file_exists($file)) {
                    echo $image_name.' No Image Exist in Folder';
                }else{
                    ?>
                    <img src="<?php echo $file; ?>" height="100" width="100" data-thumb="<?php echo $file; ?>" alt="" />
                    <?php
                }
                ?>
               </td>
            <td><?php  if($row['status'] == 0){ echo 'Inactive<br><a href="admin_image_status.php?status=1&&image_id='.$row['image_id'].'">Click to Active this Image for Frontend</a>';}else{  echo 'Active<br><a href="admin_image_status.php?status=0&&image_id='.$row['image_id'].'">Click to Inactive this Image for Frontend</a>';}?></td>

            <td><?php  if($row['header_slider'] == 0){ echo 'Inactive<br><a href="admin_image_status.php?header_slider=1&&image_id='.$row['image_id'].'">Click to Include to  Frontend Gallery</a>';}else{  echo 'Active<br><a href="admin_image_status.php?header_slider=0&&image_id='.$row['image_id'].'">Click to Exclude from  Frontend Gallery</a>';}?></td>
            <td> <a href="admin_image_gallery_delete.php?id=<?php echo $row['image_id']?>&&image_name=<?php echo $row['image_name']?>" onclick="return confirm('Are you sure you want to delete this Image?');" class="nav-link">
                    <i class="icon icon-target"></i> Delete
                </a></td>

        </tr>
    <?php }
}
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
