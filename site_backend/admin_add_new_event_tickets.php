<?php
require_once('header.php');
require_once('../Picqer/src/BarcodeGenerator.php');
require_once('../Picqer/src/BarcodeGeneratorPNG.php');
require_once('../Picqer/src/BarcodeGeneratorSVG.php');
require_once('../Picqer/src/BarcodeGeneratorJPG.php');
require_once('../Picqer/src/BarcodeGeneratorHTML.php');

function random_num($size) {
    $alpha_key = '';
    $keys = range('A', 'Z');

    for ($i = 0; $i < 2; $i++) {
        $alpha_key .= $keys[array_rand($keys)];
    }

    $length = $size - 2;

    $key = '';
    $keys = range(0, 9);

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $alpha_key . $key;
}



if(isset($_POST['admin_add_new_event_tickets_submit'])){
$error=0;
if(empty($_POST['event_tickets_event_id'])){
    $error=1;
}
if(empty($_POST['event_tickets_number_of_tickets'])){
    $error=1;
}
    if(empty($_POST['event_tickets_customer_id'])){
        $error=1;
    }
    $createEventFolder = '../Booked_Tickets/event_id_'.$_POST['event_tickets_event_id'].'/';
    $showTicket ='/Booked_Tickets/event_id_'.$_POST['event_tickets_event_id'].'/';
    if (!file_exists($createEventFolder)) {
        mkdir($createEventFolder, 0777, true);
    }
   $ticketString= random_num(9);
    $generatorHTML = new Picqer\Barcode\BarcodeGeneratorHTML();
    $ticket_code = $createEventFolder.$ticketString.'.html';
    file_put_contents($ticket_code, 'customer_id : '.$_POST['event_tickets_customer_id'].'<br>'.$generatorHTML->getBarcode($ticketString, $generatorHTML::TYPE_CODE_128));
    $showTicket ='/Booked_Tickets/event_id_'.$_POST['event_tickets_event_id'].'/'.$ticketString.'.html';
if($error == 0){

        $adminInsertQuery = "INSERT INTO event_tickets (event_id, customer_id, ticket_code,number_of_tickets,unit_price,total_billed,ticket_readable_string)
VALUES ('" . $_POST['event_tickets_event_id'] . "', '" . $_POST['event_tickets_customer_id'] . "', '" . $showTicket . "','" . $_POST['event_tickets_number_of_tickets'] . "','" . $_POST['event_tickets_unit_price'] . "','" . $_POST['event_tickets_total_billed'] . "','" . $ticketString . "')";
        mysqli_query($conn, $adminInsertQuery);
        header('Location: admin_event_tickets_grid.php');

}
}
?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Book New Ticket
                            </div>
<form method="post" action="admin_add_new_event_tickets.php" class="form-horizontal">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Select Event</label>
                                            <select onchange="window.location='<?php echo $_SERVER['SCRIPT_NAME'];?>?event_id=' + this.value" id="event_tickets_event_id" name="event_tickets_event_id" class="form-control" required>
                                                <option value="">Select</option>
                                                <?php
                                                $checkAlreadyExistQuery='select * from registered_event where event_status=1';
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>

                                                        <option value="<?php echo $data['event_id']; ?>"  <?php if(isset($_GET['event_id'])&& !empty($_GET['event_id']) && is_numeric($_GET['event_id'])&& $_GET['event_id']==$data['event_id']){ echo "selected";}?>  ><?php echo $data['name']; ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Select Number of Units</label>

                                            <select onchange="document.getElementById('event_tickets_total_billed_lbl').innerHTML=this.value*document.getElementById('event_tickets_unit_price').value;document.getElementById('event_tickets_total_billed_inp').value=this.value*document.getElementById('event_tickets_unit_price').value;" name="event_tickets_number_of_tickets" class="form-control" required>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Select Customer</label>
                                            <select id="single-select" name="event_tickets_customer_id" class="form-control" required>
                                                <option value="99999999999999">Admin</option>
                                                <?php
                                                $checkAlreadyExistQuery='select * from frontend_user where status=1';
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <option  value="<?php echo $data['id']; ?>"><?php echo $data['user_name']; ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Event Unit Price</label>
                                            <?php
                                            if(isset($_GET['event_id'])&& !empty($_GET['event_id']) && is_numeric($_GET['event_id'])) {


                                                $checkAlreadyExistQuery='select price from registered_event where event_status=1 and event_id ='.$_GET['event_id'];
                                                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                                                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                                                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                                                        ?>
                                                        <p class="form-control-plaintext"><?php echo $data['price']?></p>
                                                        <input type="hidden" id="event_tickets_unit_price" name="event_tickets_unit_price" value="<?php echo $data['price'];?>">
                                                        <?php
                                                    } }

                                            }


                                            ?>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Total Billed</label>
                                            <p id="event_tickets_total_billed_lbl" class="form-control-plaintext"></p>
                                            <input id="event_tickets_total_billed_inp"  type="hidden"  name="event_tickets_total_billed" value="">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" name="admin_add_new_event_tickets_submit" value="Book Ticket">
								<button><td><a href="index.php">cancel</a></td></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
