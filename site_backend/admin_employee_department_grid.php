<?php require_once('header.php'); ?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Employee Department
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Department</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
<?php $adminSelectQuery = 'select * from employee_department';
$adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
    <tr>
        <td colspan="6" align="center">No Data Found</td>
    </tr>
<?php }else {
    while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
        ?>
        <tr>
            <td><?php echo $row['department_id']?></td>
            <td class="text-nowrap"><?php echo $row['name']?></td>
            <td><?php echo wordwrap($row['description'],15,"<br>\n")?></td>
            <td><?php  if($row['status'] == 0){ echo 'InActive';}else{ echo 'Active';}?></td>
            <td> <a href="admin_employee_department_edit.php?id=<?php echo $row['department_id']?>" class="nav-link">
                    <i class="icon icon-target"></i> Edit
                </a></td>
            <td> <a href="admin_employee_department_delete.php?id=<?php echo $row['department_id']?>" onclick="return confirm('Are you sure you want to delete this Employee Department?');" class="nav-link">
                    <i class="icon icon-target"></i> Delete
                </a></td>
        </tr>
    <?php }
}
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
