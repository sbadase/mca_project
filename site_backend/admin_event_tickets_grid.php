<?php require_once('header.php'); ?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Booked Tickets
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Ticket ID</th>
                                        <th>Customer ID/Name</th>
                                        <th>Event ID/Name</th>
                                        <th>Ticket Code</th>
                                        <th>Ticket Count</th>
                                        <th>Ticket Unit Price</th>
                                        <th>Total billed</th>
                                    </tr>
                                    </thead>
                                    <tbody>
<?php $adminSelectQuery = 'select * from event_tickets';
$adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
    <tr>
        <td colspan="6" align="center">No Data Found</td>
    </tr>
<?php }else {
    while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
        ?>
        <tr>
            <td><?php echo $row['ticket_id']?></td>
            <td class="text-nowrap">
                <?php

                $checkAlreadyExistQuery='select * from frontend_user where status=1 and id ='.$row['customer_id'];
                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                        ?>
                        <p class="form-control-plaintext"><?php echo $row['customer_id'].'/'.$data['user_name']?></p>
                        <?php
                 } }  ?>
                </td>
            <td class="text-nowrap">
                <?php

                $checkAlreadyExistQuery='select * from registered_event where event_status=1 and event_id ='.$row['event_id'];
                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                        ?>
                        <p class="form-control-plaintext"><?php echo $row['event_id'].'/'.$data['name']?></p>
                        <?php
                    } }  ?>
            </td>
            <td id="printablediv_<?php echo $row['ticket_id'] ?>"><?php echo $row['ticket_readable_string'] ?><br><iframe src="../<?php echo $row['ticket_code'] ?>"></iframe><br>
                <input type="button" value="Print Ticket" onclick="javascript:printDiv('printablediv_<?php echo $row['ticket_id'] ?>')" /></td>
            <td><?php echo $row['number_of_tickets']; ?></td>
            <td><?php echo $row['unit_price']; ?></td>
            <td><?php echo $row['total_billed']; ?></td>
        </tr>
    <?php }
}
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?>
<script type="text/javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;
        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
        //Print Page
        window.print();
        //Restore orignal HTML
        document.body.innerHTML = oldPage;

    }


</script>
</body>
</html>
