<?php require_once('header.php');

if(isset($_GET['id'])
    && !empty($_GET['id'])
    && is_numeric($_GET['id'])){
    $adminSelectedAccountCheckQuery = 'select *  from backend_user where id= '.$_GET['id'];
    $adminSelectedAccountCheckQueryResult = mysqli_query($conn, $adminSelectedAccountCheckQuery);
    $data = mysqli_fetch_assoc($adminSelectedAccountCheckQueryResult);
    $admin_email = $data['email'];
    $admin_account_status = $data['status'];
}

if(isset($_POST['admin_add_new_submit'])){
    $error=0;
    if(empty($_POST['admin_password'])){
        $error=1;
    }
    if($error == 0){
 $adminUpdateQuery = "UPDATE backend_user SET status='".$_POST['account_status']."',password = '".md5($_POST['admin_password'])."' WHERE id=".$_GET['id'];
            mysqli_query($conn, $adminUpdateQuery);
            header('Location: admin_account_grid.php');
    }else{

        ?>
        <script>
            alert("Please Fill Compulsory Fields ");
        </script>
        <?php
    }
}

?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                Edit Admin Data
                            </div>
                            <form method="post" action="admin_edit.php?id=<?php echo $_GET['id']?>" class="form-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Admin Email</label>
                                                <p class="form-control-plaintext"><?php echo $admin_email?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Admin Password</label>
                                                <input id="normal-input" name="admin_password"  class="form-control"  required>
                                            </div>
                                        </div>
                                        <div class="col md-4">
                                            <div class="form-group">
                                                <label for="single-select">Account Status</label>
                                                <select id="single-select" name="account_status" class="form-control" required>
                                                    <option value="0" <?php if($admin_account_status==0){ echo "selected";}?> >InActive</option>
                                                    <option value="1" <?php if($admin_account_status==1){ echo "selected";}?>>Active</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input type="submit" name="admin_add_new_submit" value="Update Admin Data">
									<button><td><a href="admin_account_grid.php">cancel</a></td></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
