<?php require_once('header.php');

if(isset($_GET['id'])
    && !empty($_GET['id'])
    && is_numeric($_GET['id'])){
    $selectedAccountCheckQuery = 'select *  from event_category where category_id= '.$_GET['id'];
    $selectedAccountCheckQueryResult = mysqli_query($conn, $selectedAccountCheckQuery);
    $data = mysqli_fetch_assoc($selectedAccountCheckQueryResult);
    $event_category_name = $data['name'];
    $event_category_description = $data['description'];
    $event_category_status = $data['status'];
}

if(isset($_POST['admin_edit_event_category_submit'])){
    $error=0;
    if(empty($_POST['category_description'])){
        $error=1;
    }
    if($error == 0){
 $updateQuery = "UPDATE event_category SET status='".$_POST['event_category_status']."',description = '".$_POST['category_description']."' WHERE category_id=".$_GET['id'];
            mysqli_query($conn, $updateQuery);
            header('Location: admin_event_category_grid.php');
    }else{

        ?>
        <script>
            alert("Please Fill Compulsory Fields ");
        </script>
        <?php
    }
}

?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                Edit Event Category
                            </div>
                            <form method="post" action="admin_event_category_edit.php?id=<?php echo $_GET['id']?>" class="form-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Event Category Name</label>
                                                <p class="form-control-plaintext"><?php echo $event_category_name?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Event Category Description</label>
                                                <textarea id="normal-input"  name="category_description" class="form-control" required><?php echo $event_category_description?></textarea>
                                            </div>
                                        </div>
                                        <div class="col md-4">
                                            <div class="form-group">
                                                <label for="single-select">Event Category Status</label>
                                                <select id="single-select" name="event_category_status" class="form-control" required>
                                                    <option value="0" <?php if($event_category_status==0){ echo "selected";}?> >InActive</option>
                                                    <option value="1" <?php if($event_category_status==1){ echo "selected";}?>>Active</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input type="submit" name="admin_edit_event_category_submit" value="Update Event Category Data ">
									<button><td><a href="admin_event_category_grid.php">cancel</a></td></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
