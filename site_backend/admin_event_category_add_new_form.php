<?php require_once('header.php');

if(isset($_POST['admin_add_new_event_category_submit'])){
$error=0;
if(empty($_POST['category_name'])){
    $error=1;
}
if(empty($_POST['category_description'])){
    $error=1;
}
if($error == 0){
 $checkAlreadyExistQuery='select * from event_category where name = "'.$_POST['category_name'].'"';
    $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
    if (mysqli_num_rows($checkAlreadyExistQueryResult) == 1) {
        ?>
        <script>
            alert("Event Category Exist");
        </script>
    <?php
    }else {
        $insertQuery = "INSERT INTO event_category (name, description, status)
VALUES ('" . $_POST['category_name'] . "', '" . $_POST['category_description'] . "', '" . $_POST['event_category_status'] . "')";
        mysqli_query($conn, $insertQuery);
        header('Location: admin_event_category_grid.php');
    }
}
}
?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Add New Event Category
                            </div>
<form method="post" action="admin_event_category_add_new_form.php" class="form-horizontal">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Name</label>
                                            <input type="text" id="normal-input" name="category_name" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Category Description</label>
                                            <textarea id="normal-input"  name="category_description" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Event Category Status</label>
                                            <select id="single-select" name="event_category_status" class="form-control" required>
                                                <option value="0">InActive</option>
                                                <option value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" name="admin_add_new_event_category_submit" value="Add New Event Category">
								<button><td><a href="index.php">cancel</a></td></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
