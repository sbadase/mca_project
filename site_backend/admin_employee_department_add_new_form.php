<?php require_once('header.php');

if(isset($_POST['admin_add_new_employee_department_submit'])){
$error=0;
if(empty($_POST['employee_department_name'])){
    $error=1;
}
if(empty($_POST['employee_department_description'])){
    $error=1;
}
if($error == 0){
 $checkAlreadyExistQuery='select * from employee_department where name = "'.$_POST['employee_department_name'].'"';
    $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
    if (mysqli_num_rows($checkAlreadyExistQueryResult) == 1) {
        ?>
        <script>
            alert("Employee Department Exist");
        </script>
    <?php
    }else {
        $insertQuery = "INSERT INTO employee_department (name, description, status)
VALUES ('" . $_POST['employee_department_name'] . "', '" . $_POST['employee_department_description'] . "', '" . $_POST['employee_department_status'] . "')";
        mysqli_query($conn, $insertQuery);
        header('Location: admin_employee_department_grid.php');
    }
}
}
?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Add New Employee Department
                            </div>
<form method="post" action="admin_employee_department_add_new_form.php" class="form-horizontal">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label"> Employee Department Name</label>
                                            <input type="text" id="normal-input" name="employee_department_name" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Employee Department Description</label>
                                            <textarea id="normal-input"  name="employee_department_description" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Employee Department Status</label>
                                            <select id="single-select" name="employee_department_status" class="form-control" required>
                                                <option value="0">InActive</option>
                                                <option value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" name="admin_add_new_employee_department_submit" value="Add New Employee Department">
								<button><td><a href="admin_employee_department_grid.php">cancel</a></td></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
