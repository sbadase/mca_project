<?php require_once('header.php'); ?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            General or Event Specific Feed Backs
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th> FeedBack ID</th>
                                        <th> Customer ID</th>
                                        <th>Feed Back for <br>
                                            Event/General(About Site)
                                        </th>
                                        <th>Feed Back</th>
                                        <th>Visible of Frontend</th>
                                    </tr>
                                    </thead>
                                    <tbody>
<?php $adminSelectQuery = 'select * from customer_feedback_or_inquiries where event_id > 0';
$adminSelectQueryResult = mysqli_query($conn, $adminSelectQuery);
if (mysqli_num_rows($adminSelectQueryResult) == 0) { ?>
    <tr>
        <td colspan="6" align="center">No Data Found</td>
    </tr>
<?php }else {
    while ($row = mysqli_fetch_assoc($adminSelectQueryResult)) {
        ?>
        <tr>
            <td><?php echo $row['feedback_id']?></td>
            <td class="text-nowrap">
                <?php

                $checkAlreadyExistQuery='select * from frontend_user where status=1 and id ='.$row['customer_id'];
                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                        ?>
                        <p class="form-control-plaintext"><?php echo $row['customer_id'].'/'.$data['user_name']?></p>
                        <?php
                    } }  ?>
            </td>
            <td class="text-nowrap">
                <?php

                $checkAlreadyExistQuery='select * from registered_event where event_status=1 and event_id ='.$row['event_id'];
                $checkAlreadyExistQueryResult=mysqli_query($conn, $checkAlreadyExistQuery);
                if (mysqli_num_rows($checkAlreadyExistQueryResult) >0) {
                    while($data = mysqli_fetch_assoc($checkAlreadyExistQueryResult)) {
                        ?>
                        <p class="form-control-plaintext"><?php echo $row['event_id'].'/'.$data['name']?></p>
                        <?php
                    } }else{
                    ?>
                    <p class="form-control-plaintext">General(About Site)</p>
                    <?php

                }  ?>
            </td>
            <td><?php echo wordwrap($row['feedback'],15,"<br>\n")?></td>
            <td><?php  if($row['approval_status'] == 0){ echo 'No<br><a href="admin_disable_feedback_on_frontend.php?feedback_status=1&&feedback_id='.$row['feedback_id'].'">Click Enable this FeedBack on Frontend</a>';}else{  echo 'Yes<br><a href="admin_disable_feedback_on_frontend.php?feedback_status=0&&feedback_id='.$row['feedback_id'].'">Click Disable this FeedBack on Frontend</a>';}?></td>
        </tr>
    <?php }
}
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
