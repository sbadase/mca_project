<?php require_once('header.php');
if(isset($_GET['msg']) && !empty($_GET['msg'])){
    ?>
    <script>
        alert("<?php echo $_GET['msg'] ?>");
    </script>
    <?php

}
if(isset($_POST['admin_upload_gallery_image'])){

$error=0;

    if(is_null($_POST['status'])){
        $error=1;
    }
    if(is_null($_POST['header_slider'])){
        $error=1;
    }

if($error == 0){

        if(isset($_FILES['file'])){
            if($_FILES['file']['error'] > 0) { echo 'Error during uploading, try again'; }

//We won't use $_FILES['file']['type'] to check the file extension for security purpose

//Set up valid image extensions
            $extsAllowed = array( 'jpg', 'jpeg', 'png', 'gif' );

//Extract extention from uploaded file
//substr return ".jpg"
//Strrchr return "jpg"

            $extUpload = strtolower( substr( strrchr($_FILES['file']['name'], '.') ,1) ) ;
//Check if the uploaded file extension is allowed

            if (in_array($extUpload, $extsAllowed) ) {

                //Upload the file on the server
              $newFileName=  rtrim(base64_encode(md5(microtime())),"=").$_FILES['file']['name'];

                $name = "../site_frontend/images/{$newFileName}";
                $dbImageName = "{$newFileName}";
                $result = move_uploaded_file($_FILES['file']['tmp_name'], $name);
                if($result){echo "<img src='$name'/>";}

            } else {         ?>
                <script>
                    alert("Invalid File Upload Event,Event Not Registered,Please Try again");
                </script>
                <?php
                header('Location: admin_image_gallery_grid.php?msg=Invalid File Upload Event,Event Not Registered,Please Try again');
                exit;
            }


        }
        $insertQuery = "INSERT INTO image_gallery (image_name,status,header_slider)
VALUES ('" . $dbImageName . "', '" . $_POST['status'] . "', '" . $_POST['header_slider'] . "')";

        mysqli_query($conn, $insertQuery);
        header('Location: admin_image_gallery_grid.php');

}
}
?>



<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                               Upload New Image
                            </div>
<form method="post" action="admin_image_gallery_upload.php" class="form-horizontal" enctype="multipart/form-data">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="normal-input" class="form-control-label">Pick a file :</label>
                                            <input type="file" id="normal-input" name ="file">
                                        </div>
                                    </div>

                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Image Status</label>
                                            <select id="single-select" name="status" class="form-control" required>
                                                <option value="0">InActive</option>
                                                <option value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col md-4">
                                        <div class="form-group">
                                            <label for="single-select">Include in Header Gallery</label>
                                            <select id="single-select" name="header_slider" class="form-control" required>
                                                <option value="0">InActive</option>
                                                <option value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" name="admin_upload_gallery_image" value="Upload Image">
								<button><td><a href="admin_image_gallery_grid.php">cancel</a></td></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php'); ?>
<style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
<title></title>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.117/styles/kendo.material.mobile.min.css" />

<script src="https://kendo.cdn.telerik.com/2018.1.117/js/jquery.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.117/js/kendo.all.min.js"></script>
<script>
    $(document).ready(function() {
        function startChange() {
            var startDate = start.value(),
                endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.max(new Date(endDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
        }

        function endChange() {
            var endDate = end.value(),
                startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.max(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
        }

        var today = kendo.date.today();

        var start = $("#start").kendoDateTimePicker({
            value: today,
            change: startChange,
            parseFormats: ["YYYY-MM-DD HH:MM:SS"]
        }).data("kendoDateTimePicker");

        var end = $("#end").kendoDateTimePicker({
            value: today,
            change: endChange,
            parseFormats: ["YYYY-MM-DD HH:MM:SS"]
        }).data("kendoDateTimePicker");

        start.max(end.value());
        end.min(start.value());
    });
</script>
</body>

</html>
