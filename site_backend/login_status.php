<?php
require_once('../config.php');
if(empty($_SESSION)){
    session_destroy();
    header('Location: login.php');
}
if(isset($_SESSION) && isset($_SESSION['login_verify']) && $_SESSION['login_verify'] != 1){
    session_destroy();
    header('Location: login.php');
}
?>