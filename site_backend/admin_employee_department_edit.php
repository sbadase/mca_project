<?php require_once('header.php');

if(isset($_GET['id'])
    && !empty($_GET['id'])
    && is_numeric($_GET['id'])){
    $selectedAccountCheckQuery = 'select *  from employee_department where department_id= '.$_GET['id'];
    $selectedAccountCheckQueryResult = mysqli_query($conn, $selectedAccountCheckQuery);
    $data = mysqli_fetch_assoc($selectedAccountCheckQueryResult);
    $employee_department_name = $data['name'];
    $employee_department_description = $data['description'];
    $employee_department_status = $data['status'];
}

if(isset($_POST['admin_edit_employee_department_submit'])){
    $error=0;
    if(empty($_POST['employee_department_description'])){
        $error=1;
    }
    if($error == 0){
 $updateQuery = "UPDATE employee_department SET status='".$_POST['employee_department_status']."',description = '".$_POST['employee_department_description']."' WHERE department_id=".$_GET['id'];
            mysqli_query($conn, $updateQuery);
            header('Location: admin_employee_department_grid.php');
    }else{

        ?>
        <script>
            alert("Please Fill Compulsory Fields ");
        </script>
        <?php
    }
}

?>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <div class="main-container">
        <?php
        require_once('left_sidebar.php');
        ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                Edit Employee Department
                            </div>
                            <form method="post" action="admin_employee_department_edit.php?id=<?php echo $_GET['id']?>" class="form-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Employee Department Name</label>
                                                <p class="form-control-plaintext"><?php echo $employee_department_name?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Employee Department Description</label>
                                                <textarea id="normal-input"  name="employee_department_description" class="form-control" required><?php echo $employee_department_description?></p></textarea>
                                            </div>
                                        </div>
                                        <div class="col md-4">
                                            <div class="form-group">
                                                <label for="single-select">Employee Department Status</label>
                                                <select id="single-select" name="employee_department_status" class="form-control" required>
                                                    <option value="0" <?php if($employee_department_status==0){ echo "selected";}?> >InActive</option>
                                                    <option value="1" <?php if($employee_department_status==1){ echo "selected";}?>>Active</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input type="submit" name="admin_edit_employee_department_submit" value="Update Employee Department Data ">
									<button><td><a href="admin_employee_department_grid.php">cancel</a></td></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once('footer.php'); ?> </body>
</html>
